+------------------------+
| TERMINOLOGY VALIDATION |
--------------------------------------------------------------------------------------------------------------------------------

field    = MEZŐNÉV
message  = HIBAÜZENET
context  = KONTEXTUS
validate = VALIDÁCIÓ
convert  = KONVERZIÓ
msl      = Hibaüzeneteket visszafejtő objektum
valid    = ÉRVÉNYES
invalid  = ÉRVÉNYTELEN
default  = ALAPÉRTELMEZETT
--------------------------------------------------------------------------------------------------------------------------------