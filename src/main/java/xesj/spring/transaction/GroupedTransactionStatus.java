package xesj.spring.transaction;
import java.util.ArrayList;
import java.util.List;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.SimpleTransactionStatus;

/**
 * Tranzakció státuszok csoportja
 */
public class GroupedTransactionStatus extends SimpleTransactionStatus {
  
  /**
   * Státuszok listája
   */
  private final List<TransactionStatus> statusList = new ArrayList<>();

  /**
   * Új státusz hozzáadása a listához 
   */
  void add(TransactionStatus ts) {
    
    statusList.add(ts);
    
  }
  
  /**
   * Státusz lista adott elemének lekérdezése
   */
  TransactionStatus get(int i) {
    
    return statusList.get(i);
    
  }

  // ====
}
