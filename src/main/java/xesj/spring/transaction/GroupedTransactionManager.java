package xesj.spring.transaction;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 * Tranzakció menedzserek csoportja.
 * A tranzakció befejezésekor mindegyik menedzseren végrehajtja a commit-ot, vagy a rollback-et.
 */
public class GroupedTransactionManager implements PlatformTransactionManager {
  
  /**
   * Menedzserek listája
   */
  private final PlatformTransactionManager[] managers;

  /**
   * Konstruktor
   */
  public GroupedTransactionManager(PlatformTransactionManager... managers) {
    
    // Ellenőrzés
    if (managers == null || managers.length == 0) {
      throw new RuntimeException("Legalább egy tranzakció menedzser megadása kötelező!");
    }
    
    // Beállítása
    this.managers = managers;
    
  }
  
  /**
   * Tranzakció státusz lekérdezése.
   * A getTransaction() kérést delegálja a tranzakció menedzserek felé, és a válaszukat csoportba gyűjti.
   */
  @Override
  public TransactionStatus getTransaction(TransactionDefinition definition) {
    
    var groupedStatus = new GroupedTransactionStatus();
    for (var manager: managers) {
      var status = manager.getTransaction(definition);
      groupedStatus.add(status);
    }  
    return groupedStatus;
    
  }
  
  /**
   * Tranzakció commit.
   * A commit() kérést delegálja a tranzakció menedzserek felé.
   */
  @Override
  public void commit(TransactionStatus status) {
    
    var groupedStatus = (GroupedTransactionStatus)status;
    for (int i = 0; i < managers.length; i++) {
      activateSynchronization();
      managers[i].commit(groupedStatus.get(i));
    }
    
  }
  
  /**
   * Tranzakció rollback.
   * A rollback() kérést delegálja a tranzakció menedzserek felé.
   */
  @Override
  public void rollback(TransactionStatus status) {
    
    var groupedStatus = (GroupedTransactionStatus)status;
    for (int i = 0; i < managers.length; i++) {
      activateSynchronization();
      managers[i].rollback(groupedStatus.get(i));
    }
    
  }
  
  /**
   * Tranzakció szinkronizáció aktivvá tétele, ha még nem aktív.
   */
  private void activateSynchronization() {
    
    if (!TransactionSynchronizationManager.isSynchronizationActive()) {
      TransactionSynchronizationManager.initSynchronization();
    }
    
  }

  // ====  
}
