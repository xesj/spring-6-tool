package xesj.spring.validation;
import xesj.spring.validation.validate.Validate;
import xesj.spring.validation.convert.Convert;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import xesj.tool.StringTool;

/**
 * Konverzió/validáció kontextus általános esetre (nem html űrlapok esetére).
 * A mezőneveket mindig a kontextusba kerülésük sorrendjében adja vissza.
 * Egy mezőhöz tartozó hibaüzeneteket mindig a kontextusba kerülésük sorrendjében adja vissza.
 * <br/><br/>
 * Tartalmaz egy boolean típusú flag-et, ami beállítható, illetve lekérdezhető.
 * Amikor a kontextusba hibaüzenet kerül, akkor a kontextus a flag-et false értékre állítja.
 * <br/><br/>
 * A kontextushoz egy jelölő objektumot (marker-t) is be lehet állítani, és ki lehet olvasni.
 * A marker nem befolyásolja a kontextus működését.
 */
public class ValidationContext {
  
  /**
   * A hibaüzenetek maximális száma egy kitöltött mezőnévhez. 
   * Legalább 1-nek kell lennie.
   * Null esetén nincs korlátozás.
   */
  @Getter
  private Integer fieldMessageMaximum;

  /**
   * A globális hibaüzenetek maximális száma. 
   * Legalább 1-nek kell lennie.
   * Null esetén nincs korlátozás.
   */
  @Getter
  private Integer globalMessageMaximum;

  /**
   * MessageSource + Locale
   */
  private MessageSourceLocale msl;

  /**
   * Meghatározza, hogy hány hibaüzenetnek kell a kontextusba kerülni ahhoz, 
   * hogy a kontextus automatikusan ValidationContextException-t dobjon.
   * Legalább 1-nek kell lennie.
   * Null esetén nem dob a kontextus ValidationContextException-t.
   */
  @Getter
  private Integer throwCount;
  
  /**
   * Hibaüzenet tároló map. 
   * A globális hibaüzenetekhez tartozó kulcs: null.
   */
  private LinkedHashMap<String, List<String>> messageMap = new LinkedHashMap<>();

  /**
   * Hibaüzenet számláló. Tárolja, hogy hány hibaüzenet van a kontextusban.
   */
  private int messageCount;
  
  /** 
   * Konvertált adatok map-je. 
   * Ha egy mezőhöz már volt konverzió, akkor a mezőnév mint kulcs szerepel benne.
   * A mezőnév lehet null is.
   * Sikeres konverzió esetén a kulcshoz tartozó érték a konverzió eredménye.
   * Sikertelen konverzió esetén a kulcshoz tartozó érték a ConvertError osztály egy példánya. 
   */
  private Map<String, Object> convertMap = new HashMap<>();
  
  /** 
   * Konverziós hibát jelző objektum.
   */
  private static final ConvertError CONVERT_ERROR = new ConvertError();

  /**
   * Hibaüzenet jelző flag.
   * Amikor hibaüzenet kerül a kontextusba, a flag értékét a kontextus false-ra állítja.
   * Ha az értékét átállítjuk, akkor sem lesz hatása a kontextus működésére.
   * A flag értékének átállítása akkor hasznos, ha a későbbiekben a flag le van vizsgálva,
   * így kiderül, hogy a setFlag(true), és isFlag() között keletkezett-e új hibaüzenet a kontextusban. 
   */
  @Getter
  @Setter
  private boolean flag = true;

  /**
   * A kontextust megjelölő objektum. 
   * A kontextus működésére nincs hatással, csak információs célokat szolgál.
   * Tetszőlegesen felhasználható.
   */
  @Getter
  @Setter
  private Object marker;
  
  /**
   * Konverzió/validáció kontextus létrehozása.
   * @param msl 
   *        Hibaüzeneteket visszafejtő MessageSource + Locale objektum.
   *        <br/>
   * @param fieldMessageMaximum 
   *        A hibaüzenetek maximális száma egy kitöltött mezőnévhez. 
   *        Legalább 1-nek kell lennie.
   *        Null esetén nincs korlátozás.
   *        <br/>
   * @param globalMessageMaximum 
   *        A globális hibaüzenetek maximális száma. 
   *        Legalább 1-nek kell lennie.
   *        Null esetén nincs korlátozás.
   *        <br/>
   * @param throwCount 
   *        Meghatározza, hogy hány hibaüzenetnek kell a kontextusba kerülni ahhoz, 
   *        hogy a kontextus automatikusan ValidationContextException-t dobjon.
   *        Legalább 1-nek kell lennie.
   *        Null esetén nem dob a kontextus ValidationContextException-t.
   */
  public ValidationContext(
    @NonNull MessageSourceLocale msl, 
    Integer fieldMessageMaximum, 
    Integer globalMessageMaximum, 
    Integer throwCount) {

    // Ellenőrzés: fieldMessageMaximum
    if (fieldMessageMaximum != null && fieldMessageMaximum < 1) {
      throw new ValidationUsageException("A 'fieldMessageMaximum' paraméternek legalább 1-nek kell lennie!");
    }

    // Ellenőrzés: globalMessageMaximum
    if (globalMessageMaximum != null && globalMessageMaximum < 1) {
      throw new ValidationUsageException("A 'globalMessageMaximum' paraméternek legalább 1-nek kell lennie!");
    }

    // Ellenőrzés: throwCount
    if (throwCount != null && throwCount < 1) {
      throw new ValidationUsageException("A 'throwCount' paraméternek legalább 1-nek kell lennie!");
    }
    
    // Beállítás  
    this.msl = msl;
    this.fieldMessageMaximum = fieldMessageMaximum;
    this.globalMessageMaximum = globalMessageMaximum;
    this.throwCount = throwCount;

  }
  
  /**
   * Konverzió hibaüzenetének, és a konverzió eredményét felhasználó validációk hibaüzeneteinek hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenetek hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenetek globálisak lesznek. 
   * @param convert 
   *        Konverzió, például:<br/>
   *        <code>new IntegerConvert(data)</code>
   * @param functions 
   *        Functions függvények, melyek megadása lambda kifejezéssel ajánlott, például:<br/>
   *        <code>(c) -&gt; new IntegerRangeValidate((Integer)c, 1, 90)</code>
   */
  public void add(String field, @NonNull Convert convert, @NonNull Function<Object, Validate>... functions) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Konverzió hozzáadása
    add(field, convert);
    
    // Validációk hozzáadása
    for (Function<Object, Validate> function: functions) {
      add(field, function);
    }
    
  }

  /**
   * Konverzió hibaüzenetének hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param convert 
   *        Konverzió, például:<br/>
   *        <code>new IntegerConvert(data)</code>
   */
  public void add(String field, @NonNull Convert convert) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Volt már konverzió ehhez a mezőhöz ?
    if (convertMap.containsKey(field)) {
      throw new ValidationUsageException("Ezen a mezőn már volt konverzió: " + field);
    }
    
    // Van már hibaüzenet ehhez a mezőhöz ? 
    if (field == null && hasGlobalMessage() || field != null && hasFieldMessage(field)) {
      throw new ValidationUsageException("A konverzió előtt már van hibaüzenet a mezőhöz: " + field);
    }    
    
    // Konverzió eredményének vizsgálata 
    if (convert.isValid()) {
      convertMap.put(field, convert.getValue());
    }
    else {
      convertMap.put(field, CONVERT_ERROR);
      addMessage(field, convert.getMessage(msl));    
    }    

  }

  /**
   * A konverzió eredményét felhasználó validációk hibaüzeneteinek hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param functions 
   *        Functions függvények, melyek megadása lambda kifejezéssel ajánlott, például:<br/>
   *        <code>(c) -&gt; new IntegerRangeValidate((Integer)c, 1, 90)</code>
   */
  public void add(String field, @NonNull Function<Object, Validate>... functions) {

    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Validációk hozzáadása
    for (Function<Object, Validate> function: functions) {
      add(field, function);
    }
    
  }
  
  /**
   * A konverzió eredményét felhasználó validáció hibaüzenetének hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param function 
   *        Function függvény, melynek megadása lambda kifejezéssel ajánlott, például:<br/>
   *        <code>(c) -&gt; new IntegerRangeValidate((Integer)c, 1, 90)</code>
   */
  private void add(String field, @NonNull Function<Object, Validate> function) {

    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Volt konverzió ehhez a mezőhöz ?
    if (!convertMap.containsKey(field)) {
      throw new ValidationUsageException(
        "Ezen a mezőn nem volt konverzió, ezért a konverzió eredményét a validáció nem tudja felhasználni: " + field
      );
    }
    
    // Ha hibás volt a konverzió, akkor nem futhat a validáció
    Object convertValue = convertMap.get(field);
    if (convertValue instanceof ConvertError) {
      return; // Nincs teendő
    }
    
    // Ha a mezőhöz nem tartozhat több hibaüzenet, akkor nem futhat a validáció
    if (!enableAddMessage(field)) {
      return; // Nem futhat a validáció
    }

    // Validáció futtatása
    Validate validate = function.apply(convertValue);
    
    // A validáció lehetséges hibaüzenetének hozzáadása a mezőhöz
    addMessage(field, validate.getMessage(msl));    

  }

  /**
   * Validációk hibaüzeneteinek hozzáadása a kontextushoz. 
   * A validációkat nem előzheti meg konverzió.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param suppliers 
   *        Supplier függvények, melyek megadása lambda kifejezéssel ajánlott, például:<br/>
   *        <code>() -&gt; new IntegerRangeValidate(data, 1, 90)</code>
   */
  public void add(String field, @NonNull Supplier<Validate>... suppliers) {

    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Validációk hozzáadása
    for (Supplier<Validate> supplier: suppliers) {
      add(field, supplier);
    }
    
  }
  
  /**
   * Validáció hibaüzenetének hozzáadása a kontextushoz. 
   * A validációkat nem előzheti meg konverzió.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param supplier 
   *        Supplier függvény, mely megadása lambda kifejezéssel ajánlott, például:<br/>
   *        <code>() -&gt; new IntegerRangeValidate(data, 1, 90)</code>
   */
  private void add(String field, @NonNull Supplier<Validate> supplier) {

    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Volt konverzió ehhez a mezőhöz ?
    if (convertMap.containsKey(field)) {
      throw new ValidationUsageException("Ezen a mezőn volt konverzió: " + field);
    }
    
    // Ha a mezőhöz nem tartozhat több hibaüzenet, akkor nem futhat a validáció
    if (!enableAddMessage(field)) {
      return; // Nem futhat a validáció
    }

    // Validáció futtatása
    Validate validate = supplier.get();
    
    // A validáció lehetséges hibaüzenetének hozzáadása a mezőhöz
    addMessage(field, validate.getMessage(msl));    

  }

  /**
   * Hibaüzenet objektum hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param message 
   *        Hibaüzenet objektum. Kötelező kitölteni.
   */
  public void add(String field, @NonNull Message message) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Hibaüzenet hozzáadása a mezőhöz, ha engedélyezett
    if (enableAddMessage(field)) {
      addMessage(field, message.getMessageText(msl));
    }  

  }

  /**
   * Hibaüzenet szöveg hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz.
   * @param message 
   *        Hibaüzenet szöveg. Kötelező kitölteni.
   */
  public void add(String field, @NonNull String message) {
    
    // Ellenőrzés: mezőnév
    checkField(field);

    // Ellenőrzés: hibaüzenet
    checkMessage(message);

    // Hibaüzenet hozzáadása a mezőhöz, ha engedélyezett
    if (enableAddMessage(field)) {
      addMessage(field, message);
    }  

  }
  
  /**
   * Belső eljárás: hibaüzenet szöveg hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param message 
   *        Hibaüzenet szöveg.
   */
  private void addMessage(String field, String message) {
    
    // Ellenőrzés: mezőnév
    checkField(field);

    // Ki van töltve a hibaüzenet ?
    if (StringTool.isNullOrEmpty(message)) {
      return;
    }
    
    // Hibaüzenet bejegyzése
    if (messageMap.containsKey(field)) {
      messageMap.get(field).add(message);
    }
    else {
      List<String> list = new ArrayList<>();
      list.add(message);
      messageMap.put(field, list);
    }
    
    // Hibaüzenet számláló növelése
    messageCount++;

    // Flag beállítása
    flag = false;
    
    // Ha a hibaüzenetek száma elérte a throwCount-tal beállított értéket, 
    // akkor ValidationContextException-t kell dobni.
    if (throwCount != null && throwCount <= messageCount) {
      throw new ValidationContextException(this);
    }

  }
  
  /**
   * A kontextusban lévő mezőnevek lekérdezése, a kontextusba kerülésük sorrendjében.
   * Egy mezőnév csak egyszer szerepel a listában.  
   * Ha nincs egyetlen mezőnév sem, akkor üres listát ad vissza.
   * @param global 
   *        True: ha van mezőnév null értékkel (global), akkor azt is visszaadja.
   *        False: nem adja vissza a null értékű mezőnevet.
   */
  public List<String> getFields(boolean global) {
    
    return messageMap.keySet()
      .stream()
      .filter(e -> (global ? true : e != null))
      .collect(Collectors.toList());

  }

  /**
   * Egy mezőnévhez tartozó hibaüzenetek lekérdezése, a kontextusba kerülésük sorrendjében.
   * Ha nincs ilyen, akkor üres listát ad vissza.
   * @param field 
   *        Mezőnév. Ha nincs kitöltve, akkor a globális hibaüzeneteket adja vissza.
   */
  public List<String> getMessages(String field) {
    
    // Ellenőrzés: mezőnév
    checkField(field);

    // Válasz
    return messageMap.getOrDefault(field, new ArrayList<>());
    
  }
  
  /**
   * Létezik-e bármilyen (mezőhöz rendelt, vagy globális) hibaüzenet a kontextusban ?
   */
  public boolean hasMessage() {

    return !messageMap.isEmpty();

  }

  /**
   * Létezik-e mezőhöz rendelt (nem globális) hibaüzenet a kontextusban ?
   */
  public boolean hasFieldMessage() {
    
    return !getFields(false).isEmpty();

  }
  
  /**
   * Létezik-e az adott, nem üres mezőnévhez tartozó hibaüzenet a kontextusban ?
   * @param field Kitöltött mezőnév.
   */
  public boolean hasFieldMessage(@NonNull String field) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Válasz
    return messageMap.containsKey(field);

  }
  
  /**
   * Létezik-e globális hibaüzenet a kontextusban ?
   */
  public boolean hasGlobalMessage() {

    // Válasz
    return messageMap.containsKey(null);

  }
  
  /**
   * ValidationContextException dobása ha a kontextus tartalmaz hibaüzenetet (piszkos).
   * Ha a kontextus nem tartalmaz hibaüzenetet (tiszta), akkor a metódusnak nincs hatása.
   */
  public void throwExceptionIfDirty() throws ValidationContextException {

    if (hasMessage()) {
      throw new ValidationContextException(this);
    }

  }
  
  /**
   * A kontextusban tárolt mezőknek, és hibaüzeneteiknek string-re alakítása. 
   * Ha egyetlen hibaüzenet sincs, akkor üres stringet ad vissza.
   * A mezőnevek sorrendje megegyezik a kontextusba kerülésük sorrendjével.
   * Egy mezőn belül a hibaüzenetek sorrendje megegyezik a kontextusba kerülésük sorrendjével. 
   * A globális hibaüzenetek utoljára kerülnek be a string-be, és nem lesz előttük mezőnév. Példa:
   * <pre>
   *   mező1: Nem egész szám
   *   mező2: Nem lehet negatív szám
   *   mező2: Nem páros szám
   *   Ezekkel az adatokkal nem lehet számítást végezni
   * </pre>
   */  
  @Override
  public String toString() {

    // Előkészítés
    StringBuilder sb = new StringBuilder();
    
    // Mezőhöz tartozó hibaüzenetek
    for (String field: getFields(false)) {
      for (String message: getMessages(field)) {
        if (sb.length() > 0) {
          sb.append('\n');
        }
        sb.append(field).append(": ").append(message);
      }
    }

    // Globális hibaüzenetek
    for (String message: getMessages(null)) {
      if (sb.length() > 0) {
        sb.append('\n');
      }
      sb.append(message);
    }

    // Válasz
    return sb.toString();

  }

  /**
   * Eldönti, hogy hozzáadható-e új hibaüzenet a mezőnévhez.
   * Figyelembe veszi a konverziós hibát, a fieldMessageMaximum, és a globalMessageMaximum értékeit.
   * @param field 
   *        Mezőnév. Nem kötelező kitölteni.
   */
  public boolean enableAddMessage(String field) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Ha a mezőn konverziós hiba volt, akkor nem adható hozzá újabb üzenet
    if (convertMap.get(field) instanceof ConvertError) {
      return false; 
    }
    
    // Vizsgálat, ha a mezőnév ki van töltve
    if (field != null) {
      if (fieldMessageMaximum != null && fieldMessageMaximum <= getMessages(field).size()) {
        return false; 
      }  
    }  
    
    // Vizsgálat, ha a mezőnév nincs kitöltve
    if (field == null) {
      if (globalMessageMaximum != null && globalMessageMaximum <= getMessages(null).size()) {
        return false; 
      }  
    }  
    
    // Hozzáadható új hibaüzenet    
    return true; 

  }
  
  /**
   * Az adott mezőhöz volt konverzió ?
   * @param field 
   *        Mezőnév. Nem kötelező kitölteni.
   * @param realValue 
   *        True: Az adott mezőhöz volt sikeres konverzió ?
   *        False: Az adott mezőhöz volt bármilyen (sikeres vagy sikertelen) konverzió ?
   */
  public boolean hasConvertValue(String field, boolean realValue) {
    
    // Ellenőrzés: mezőnév
    checkField(field);

    // Ha nem volt még konverzió
    if (!convertMap.containsKey(field)) {
      return false;
    }  

    // Ha hibás volt a konverzió, de a valós konverziós értékre kérdez
    if (realValue && convertMap.get(field) instanceof ConvertError) {
      return false;
    }  

    // Igaz válasz
    return true;

  } 

  /**
   * Az adott mezőhöz tartozó konverzió eredményének lekérdezése.
   * A metódus csak akkor hívható, ha a mezőhöz volt már konverzió (ez eldönthető egy hasConvertValue() hívással).
   * Ha a konverzió sikertelen volt, akkor egy ConvertError objektumot ad vissza.
   * @param field 
   *        Mezőnév. Nem kötelező kitölteni.
   * @throws ValidationUsageException 
   *         Ha a mezőhöz még nem volt konverzió.
   */
  public Object getConvertValue(String field) {
    
    // Ellenőrzés: mezőnév
    checkField(field);

    // Ellenőrzés: volt konverzió ?
    if (!hasConvertValue(field, false)) {
      throw new ValidationUsageException("A mezőhöz nem volt konverzió: " + field);
    }    
    
    // Válasz
    return convertMap.get(field);

  } 

  /**
   * Mezőnév ellenőrzése: nem lehet üres string. 
   * Üres string esetén ValidationUsageException-t dob.
   */
  public static void checkField(String field) {
    
    if (field != null && field.isEmpty()) {
      throw new ValidationUsageException("A mezőnév nem lehet üres string!");
    }
    
  }
  
  /**
   * Hibaüzenet ellenőrzése. 
   * Kitöltöttnek kell lennie, különben ValidationUsageException-t dob.
   */
  public static void checkMessage(String message) {
    
    if (StringTool.isNullOrEmpty(message)) {
      throw new ValidationUsageException("A hibaüzenet kitöltése kötelező!");
    }
    
  }
  
  /**
   * A kontextusban tárolt mezőknek, és hibaüzeneteiknek List-re alakítása. 
   * Ha egyetlen hibaüzenet sincs, akkor üres listát ad vissza.
   * A mezőnevek sorrendje megegyezik a kontextusba kerülésük sorrendjével.
   * Egy mezőn belül a hibaüzenetek sorrendje megegyezik a kontextusba kerülésük sorrendjével. 
   * A globális hibaüzenetek utoljára kerülnek be a listába, és nem lesz előttük mezőnév. 
   * Példa egy 4 elemű listára, ha a separator paraméter értéke: " --> ": 
   * <pre>
   *   mező1 --> Nem egész szám
   *   mező2 --> Nem lehet negatív szám
   *   mező2 --> Nem páros szám
   *   Ezekkel az adatokkal nem lehet számítást végezni
   * </pre>
   * @param separator 
   *        A mezőt és hozzátartozó hibaüzenetet elválasztó szöveg. Null esetén egyenértékű az üres string-gel. 
   */  
  public List<String> toList(String separator) {

    // Előkészítés
    List<String> list = new ArrayList<>();
    
    // Separator konverziója ha szükséges
    if (separator == null) {
      separator = "";
    }
    
    // Mezőhöz tartozó hibaüzenetek
    for (String field: getFields(false)) {
      for (String message: getMessages(field)) {
        list.add(field + separator + message);
      }
    }

    // Globális hibaüzenetek
    for (String message: getMessages(null)) {
      list.add(message);
    }

    // Válasz
    return list;

  }  
  
  // ====
}
