package xesj.spring.validation;
import lombok.Getter;

/**
 * A konverziót, és validációt kezelő osztályok helytelen használatakor váltódik ki.
 */
@Getter
public class ValidationUsageException extends RuntimeException {
  
  /**
   * Konstruktor
   */
  public ValidationUsageException(String message) {

    super(message);

  }
  
  // ====
}
