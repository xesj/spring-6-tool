package xesj.spring.validation;

/**
 * Konverzió/validáció során keletkező hibaüzenet kezelése.
 */
public class Message {
  
  /** 
   * A messages.properties fájlban kód hiányára utaló üzenet eleje 
   */
  public static final String NOT_EXIST = "A messages.properties fájlban hiányzik a kód:";
  
  /** 
   * A messages.properties fájlban lévő hibaüzenet kulcsa 
   */
  private String messageCode;

  /** 
   * A messages.properties fájlban lévő hibaüzenet argumentumok: {0}, {1}, ... értékei 
   */
  private Object[] messageArgs;
  
  /** 
   * A hibaüzenet szövege 
   */
  private String messageText;
  
  /**
   * Konstruktor.
   * A messageCode és messageText közül pontosan az egyik megadása kötelező.
   * @param messageCode A messages.properties fájlban lévő hibaüzenet kulcsa.
   * @param messageArgs A messages.properties fájlban lévő hibaüzenet argumentumok.
   * @param messageText Hibaüzenet szövege.
   */
  public Message(String messageCode, Object[] messageArgs, String messageText) {

    // Ellenőrzés
    if (messageCode == null && messageText == null) {
      throw new ValidationUsageException("A messageCode és messageText paraméter közül az egyik megadása kötelező!");
    }
    if (messageCode != null && messageText != null) {
      throw new ValidationUsageException("A messageCode és messageText paraméter közül mindkettő megadása tilos!");
    }
    
    // Beállítás
    this.messageCode = messageCode;
    this.messageArgs = messageArgs;
    this.messageText = messageText;

  }
  
  /**
   * A végleges hibaüzenet szöveg lekérdezése.
   * @param msl Hibaüzeneteket visszafejtő objektum.
   */
  public String getMessageText(MessageSourceLocale msl) {

    String text;
    if (messageCode != null) {
      String defaultMessage = NOT_EXIST + " '" + messageCode + "'";  
      text = msl.getMessageSource().getMessage(messageCode, messageArgs, defaultMessage, msl.getLocale());
    }
    else {
      text = messageText;
    }
    return text;

  }
  
  // ====
}
