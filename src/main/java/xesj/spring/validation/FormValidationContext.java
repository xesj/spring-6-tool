package xesj.spring.validation;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;
import lombok.Getter;
import xesj.spring.validation.validate.Validate;
import lombok.NonNull;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import static xesj.spring.validation.ValidationContext.checkField;
import static xesj.spring.validation.ValidationContext.checkMessage;
import xesj.tool.StringTool;

/**
 * Html űrlapra (form-ra) vonatkozó konverzió/validáció kontextus.
 */
public class FormValidationContext {
  
  /**
   * A Spring által készített form post-hoz tartozó BindingResult.
   */
  @Getter
  private BindingResult bindingResult;
  
  /**
   * A hibaüzenetek maximális száma egy kitöltött mezőnévhez. 
   * Legalább 1-nek kell lennie.
   * Null esetén nincs korlátozás.
   */
  @Getter
  private Integer fieldMessageMaximum;
  
  /**
   * A globális hibaüzenetek maximális száma. 
   * Legalább 1-nek kell lennie.
   * Null esetén nincs korlátozás.
   */
  @Getter
  private Integer globalMessageMaximum;
  
  /**
   * Hibaüzeneteket visszafejtő MessageSource + Locale objektum.
   */
  private MessageSourceLocale msl;
  
  /** 
   * Azoknak a mezőneveknek a halmaza, 
   * melyeket a BindingResult szerint nem lehetett konvertálni. 
   */
  private Set<String> convertErrorSet = new HashSet<>();
  
  /**
   * Html űrlapra (form-ra) vonatkozó konverzió/validáció kontextus létrehozása.
   * @param bindingResult 
   *        A Spring által készített form post-hoz tartozó BindingResult.
   *        <br/>
   * @param msl 
   *        Hibaüzeneteket visszafejtő MessageSource + Locale objektum.
   *        <br/>
   * @param fieldMessageMaximum 
   *        A hibaüzenetek maximális száma egy kitöltött mezőnévhez. 
   *        Legalább 1-nek kell lennie.
   *        Null esetén nincs korlátozás.
   *        <br/>
   * @param globalMessageMaximum 
   *        A globális hibaüzenetek maximális száma. 
   *        Legalább 1-nek kell lennie.
   *        Null esetén nincs korlátozás.
   */
  public FormValidationContext(
    @NonNull BindingResult bindingResult, 
    @NonNull MessageSourceLocale msl, 
    Integer fieldMessageMaximum, 
    Integer globalMessageMaximum) {
    
    // Ellenőrzés: fieldMessageMaximum
    if (fieldMessageMaximum != null && fieldMessageMaximum < 1) {
      throw new ValidationUsageException("A 'fieldMessageMaximum' paraméternek legalább 1-nek kell lennie!");
    }

    // Ellenőrzés: globalMessageMaximum
    if (globalMessageMaximum != null && globalMessageMaximum < 1) {
      throw new ValidationUsageException("A 'globalMessageMaximum' paraméternek legalább 1-nek kell lennie!");
    }
    
    // Példányváltozók beállítása
    this.bindingResult = bindingResult;
    this.msl = msl;
    this.fieldMessageMaximum = fieldMessageMaximum;
    this.globalMessageMaximum = globalMessageMaximum;
    
    // ConvertErrorSet beállítása: azon mezőnevek tárolása melyekre hibás volt a konverzió.
    for (FieldError fieldError: bindingResult.getFieldErrors()) {
      if (fieldError.isBindingFailure()) {
        convertErrorSet.add(fieldError.getField());
      }
    }
    
  }

  /**
   * Validációk hibaüzeneteinek hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param suppliers 
   *        Supplier függvények, melyek megadása lambda kifejezéssel ajánlott, például:<br/>
   *        <code>() -&gt; new IntegerRangeValidate(data, 1, 90)</code>
   */
  public void add(String field, @NonNull Supplier<Validate>... suppliers) {

    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Validációk hozzáadása
    for (Supplier<Validate> supplier: suppliers) {
      add(field, supplier);
    }
    
  }
  
  /**
   * Validáció hibaüzenetének hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param supplier 
   *        Supplier függvény, melyek megadása lambda kifejezéssel ajánlott, például:<br/>
   *        <code>() -&gt; new IntegerRangeValidate(data, 1, 90)</code>
   */
  private void add(String field, @NonNull Supplier<Validate> supplier) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Ha a mezőn konverziós hiba volt, akkor nem futhat a validáció
    if (convertErrorSet.contains(field)) {
      return; // Nem futhat a validáció
    }
    
    // Ha a mezőhöz nem tartozhat több hibaüzenet, akkor nem futhat a validáció
    if (!enableAddMessage(field)) {
      return; // Nem futhat a validáció
    }

    // Validáció futtatása
    Validate validate = supplier.get();
    
    // A validáció lehetséges hibaüzenetének hozzáadása a mezőhöz
    addMessage(field, validate.getMessage(msl));    

  }

  /**
   * Hibaüzenet objektum hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param message 
   *        Hibaüzenet objektum. Kötelező kitölteni.
   */
  public void add(String field, @NonNull Message message) {
    
    // Ellenőrzés: mezőnév
    checkField(field);

    // Hibaüzenet hozzáadása a mezőhöz, ha engedélyezett
    if (enableAddMessage(field)) {
      addMessage(field, message.getMessageText(msl));
    }  

  }
  
  /**
   * Hibaüzenet szöveg hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param message 
   *        Hibaüzenet szöveg. Kötelező kitölteni.
   */
  public void add(String field, @NonNull String message) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Ellenőrzés: hibaüzenet
    checkMessage(message);

    // Hibaüzenet hozzáadása a mezőhöz, ha engedélyezett
    if (enableAddMessage(field)) {
      addMessage(field, message);
    }  
    
  }
  
  /**
   * Belső eljárás: hibaüzenet szöveg hozzáadása a kontextushoz.
   * @param field 
   *        Hibaüzenet hozzárendelése ehhez a mezőnévhez. 
   *        Ha a mezőnév nincs kitöltve, akkor a hibaüzenet globális lesz. 
   * @param message 
   *        Hibaüzenet szöveg.
   */
  private void addMessage(String field, String message) {
    
    // Ellenőrzés: mezőnév
    checkField(field);

    // Ki van töltve a hibaüzenet ?
    if (StringTool.isNullOrEmpty(message)) {
      return;
    }
    
    // Hibaüzenet bejegyzése
    if (field == null) {
      bindingResult.reject(null, message);
    }
    else {
      bindingResult.rejectValue(field, null, message);
    }

  }
  
  /**
   * Eldönti, hogy hozzáadható-e új hibaüzenet a mezőnévhez.
   * Figyelembe veszi a konverziós hibát, a fieldMessageMaximum, és a globalMessageMaximum értékeit.
   * @param field 
   *        Mezőnév. Nem kötelező kitölteni.
   */
  public boolean enableAddMessage(String field) {
    
    // Ellenőrzés: mezőnév
    checkField(field);
    
    // Ha a mezőn konverziós hiba volt, akkor nem adható hozzá újabb üzenet
    if (convertErrorSet.contains(field)) {
      return false;
    }
    
    // Vizsgálat, ha a mezőnév ki van töltve
    if (field != null) {
      if (fieldMessageMaximum != null && fieldMessageMaximum <= bindingResult.getFieldErrorCount(field)) {
        return false; 
      }  
    }  
    
    // Vizsgálat, ha a mezőnév nincs kitöltve
    if (field == null) {
      if (globalMessageMaximum != null && globalMessageMaximum <= bindingResult.getGlobalErrorCount()) {
        return false; 
      }  
    }  
    
    // Hozzáadható új hibaüzenet    
    return true; 

  }

  // ====
}
