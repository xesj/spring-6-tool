package xesj.spring.validation;
import java.util.Locale;
import lombok.Getter;
import org.springframework.context.MessageSource;

/**
 * Hibaüzeneteket visszafejtéséhez szükséges objektumokat összefogó osztály
 */
@Getter
public class MessageSourceLocale {
  
  private MessageSource messageSource;
  private Locale locale;
  
  /**
   * Konstruktor
   */
  public MessageSourceLocale(MessageSource messageSource, Locale locale) {

    this.messageSource = messageSource;
    this.locale = locale;

  }
  
  // ====
}
