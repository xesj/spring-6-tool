package xesj.spring.validation;
import lombok.Getter;

/**
 * ValidationContext osztály által kiváltott exception, 
 * mely tartalmazza a ValidationContext objektumot.
 */
@Getter
public class ValidationContextException extends RuntimeException {
  
  private ValidationContext validationContext;
  
  /**
   * Konstruktor
   */
  public ValidationContextException(ValidationContext validationContext) {

    super(validationContext.toString());
    this.validationContext = validationContext;

  }
  
  // ====
}
