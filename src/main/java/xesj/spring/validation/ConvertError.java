package xesj.spring.validation;

/**
 * Konverziós hiba jelzése.
 * A ValidationContext osztály használja.
 * Ha a konverzió sikertelen, akkor ennek az osztálynak egy példánya kerül be a ValidationContext osztály convertMap tagjába.
 */
public class ConvertError {
}