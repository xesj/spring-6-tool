package xesj.spring.validation.validate;
import xesj.spring.validation.Message;
import xesj.tool.StringTool;

/**
 * String típusú adat ellenőrzése: az engedélyezett karakterekből áll-e ?
 */
public class CharacterContentValidate extends Validate {

  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: ha az adat nem null, és tartalmaz hibás karaktert.<br/> 
   * Érvényes: ha az adat null, vagy nincsenek engedélyezett karakterek, 
   *           vagy az adat csak az engedélyezett karaktereket tartalmazza.
   * @param data Vizsgálandó adat.
   * @param enabledCharacters Engedélyezett karakterek.
   */
  public CharacterContentValidate(String data, String enabledCharacters) { 

    // Nincs adat vagy engedélyezett karakter ?
    if (StringTool.isNullOrEmpty(data) || StringTool.isNullOrEmpty(enabledCharacters)) {
      return;
    }
    
    // Validáció
    for (int i = 0; i < data.length(); i++) {
      char c = data.charAt(i);
      if (enabledCharacters.indexOf(c) == -1) {
        Character badCharacter = c;
        Integer badCharacterPosition = i + 1;
        message = new Message(
          "xesj.spring.validation.CharacterContentValidate", 
          new String[]{badCharacterPosition.toString(),  badCharacter.toString()}, 
          null
        );
        return;
      } 
    }

  }
  
  // ====
}
