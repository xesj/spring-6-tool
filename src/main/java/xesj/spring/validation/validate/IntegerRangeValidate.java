package xesj.spring.validation.validate;
import xesj.spring.validation.Message;

/**
 * Integer típusú adat ellenőrzése: a megadott intervallumban van-e ?
 */
public class IntegerRangeValidate extends Validate {

  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: ha az adat nem null, és nincs a megadott intervallumon belül.<br/> 
   * Érvényes: ha az adat null, vagy a megadott intervallumon belül van.
   * @param data A vizsgálandó adat.
   * @param minimum A lehetséges minimum érték. Ha nincs minimum, akkor null. 
   * @param maximum A lehetséges maximum érték. Ha nincs maximum, akkor null. 
   */
  public IntegerRangeValidate(Integer data, Integer minimum, Integer maximum) {  
    
    // Van adat ?
    if (data == null) {
      return;
    }

    // Validáció a minimum értékre
    if (minimum != null && data < minimum) {
      message = new Message(
        "xesj.spring.validation.IntegerRangeValidate.minimum", 
        new String[]{minimum.toString()}, 
        null
      );
    }

    // Validáció a maximum értékre
    if (maximum != null && data > maximum) {
      message = new Message(
        "xesj.spring.validation.IntegerRangeValidate.maximum", 
        new String[]{maximum.toString()}, 
        null
      );
    }

  }

  // ====
}
