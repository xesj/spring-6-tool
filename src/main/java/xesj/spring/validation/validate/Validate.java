package xesj.spring.validation.validate;
import lombok.NonNull;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;

/**
 * Validáció ősosztály. 
 * Validáció készítéskor ezt az osztályt kell leszármaztatni, és a leszármazott osztálynak csak a konstruktorát kell megírni.
 * A konstruktornak el kell végezni a validációt.
 * Adathiba miatti sikertelen validáció esetén a "message" változóba be kell állítani a hibaüzenetet.
 */
public class Validate {
  
  /** 
   * Hibaüzenet 
   */
  protected Message message;
  
  /**
   * A validáció során keletkezett hibaüzenet lekérdezése. Ha nem keletkezett hibaüzenet akkor null-t ad vissza.
   * @param msl Hibaüzeneteket visszafejtő objektum.
   * @return A hibaüzenet. Ha nem keletkezett hibaüzenet akkor null.
   */
  public String getMessage(@NonNull MessageSourceLocale msl) {

    return (message == null ? null : message.getMessageText(msl));

  }

  /**
   * Validáció sikerességének ellenőrzése.
   * @return True: sikeres validáció, nem keletkezett hibaüzenet.<br/>
   *         False: sikertelen validáció, keletkezett hibaüzenet.
   */
  public boolean isValid() {

    return (message == null);

  }
  
  // ====
}
