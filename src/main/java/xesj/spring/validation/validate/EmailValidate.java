package xesj.spring.validation.validate;
import java.util.regex.Pattern;
import xesj.spring.validation.Message;
import xesj.tool.StringTool;

/**
 * Email cím formátumának ellenőrzése
 */
public class EmailValidate extends Validate {
  
  /** 
   * Email formátum 
   */
  public static final Pattern EMAIL_PATTERN = 
    Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: Ha van adat, és a formátuma nem megfelelő.<br/> 
   * Érvényes: Ha nincs adat, vagy a formátuma megfelelő.
   * @param data A vizsgálandó adat.
   */
  public EmailValidate(String data) {  
    
    // Van adat ?
    if (StringTool.isNullOrEmpty(data)) {
      return;
    }

    // Validáció: megfelel a formátumnak ?
    if (!EMAIL_PATTERN.matcher(data).matches()) {
      message = new Message("xesj.spring.validation.EmailValidate", null, null);
    }

  }

  // ====
}