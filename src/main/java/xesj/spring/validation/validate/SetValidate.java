package xesj.spring.validation.validate;
import xesj.spring.validation.Message;
import xesj.spring.validation.ValidationUsageException;

/**
 * Annak ellenőrzése, hogy a vizsgálandó adat egy meghatározott adat halmaz eleme-e.
 */
public class SetValidate extends Validate {

  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: Ha van adat, van engedélyezett adat, és az adat nem eleme az engedélyezett adatok halmazának.<br/>
   * Érvényes: Ha nincs adat, vagy nincs engedélyezett adat, vagy az adat eleme az engedélyezett adatok halmazának.
   * @param data A vizsgálandó adat.
   * @param enabled Az engedélyezett adatok. Ugyanolyan típusúnak kell lenniük mint a vizsgálandó adat!
   */
  public SetValidate(Object data, Object... enabled) {

    // Enabled elemeinek ellenőrzése
    if (enabled != null) {
      for (Object e: enabled) {
        // Az enabled minden eleme kötelező
        if (e == null || e instanceof String && ((String)e).isEmpty()) {
          throw new ValidationUsageException(
            "Az 'enabled' paraméter eleme nem lehet null vagy üres string!"
          );
        }
        // Az elem típusa megyegyezik az adat típusával ?
        if (data != null) {
          if (data.getClass() != e.getClass()) {
            throw new ValidationUsageException(
              "Az 'enabled' paraméter elemének típusa nem egyezik a vizsgálandó adat típusával!"
            );
          }
        }
      }
    }

    // Van adat ?
    if (data == null || enabled == null || data instanceof String && ((String)data).isEmpty()) {
      return;
    }

    // Validáció
    for (Object e: enabled) {
      if (data.equals(e)) {
        return;
      }
    }
    message = new Message("xesj.spring.validation.SetValidate", null, null);

  }
  
  // ====
}
