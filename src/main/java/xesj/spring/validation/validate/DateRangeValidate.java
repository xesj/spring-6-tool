package xesj.spring.validation.validate;
import java.util.Date;
import xesj.spring.validation.Message;
import xesj.tool.DateTool;

/**
 * java.util.Date típusú adat ellenőrzése: a megadott intervallumban van-e ?
 */
public class DateRangeValidate extends Validate {
  
  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: ha a vizsgálandó adat nem null, és nincs a megadott intervallumban.<br/> 
   * Érvényes: Ha a vizsgálandó adat null, vagy a megadott intervallumban van.
   * @param data A vizsgálandó adat.
   * @param minimum A lehetséges minimum érték. Ha nincs minimum, akkor null. 
   * @param maximum A lehetséges maximum érték. Ha nincs maximum, akkor null. 
   * @param printPattern Dátumhiba esetén ebben a formában kell kiírni a minimális és maximális dátumot. 
   *                     Például: "yyyy.MM.dd"
   */
  public DateRangeValidate(Date data, Date minimum, Date maximum, String printPattern) {  
    
    // Nincs adat ?
    if (data == null) {
      return;
    }
    
    // Validáció a minimum értékre
    if (minimum != null && data.compareTo(minimum) < 0) {
      message = new Message(
        "xesj.spring.validation.DateRangeValidate.minimum", 
        new String[]{DateTool.format(minimum, printPattern)}, 
        null
      );
    }

    // Validáció a maximum értékre
    if (maximum != null && data.compareTo(maximum) > 0) {
      message = new Message(
        "xesj.spring.validation.DateRangeValidate.maximum", 
        new String[]{DateTool.format(maximum, printPattern)}, 
        null
      );
    }

  }
  
  // ====
}
