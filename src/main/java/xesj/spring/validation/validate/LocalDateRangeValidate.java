package xesj.spring.validation.validate;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import xesj.spring.validation.Message;

/**
 * java.time.LocalDate típusú adat ellenőrzése: a megadott intervallumban van-e ?
 */
public class LocalDateRangeValidate extends Validate {
  
  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: ha a vizsgálandó adat nem null, és nincs a megadott intervallumban.<br/> 
   * Érvényes: Ha a vizsgálandó adat null, vagy a megadott intervallumban van.
   * @param data A vizsgálandó adat.
   * @param minimum A lehetséges minimum érték. Ha nincs minimum, akkor null. 
   * @param maximum A lehetséges maximum érték. Ha nincs maximum, akkor null. 
   * @param printPattern Dátumhiba esetén ebben a formában kell kiírni a minimális és maximális dátumot. 
   *                     Például: "uuuu.MM.dd"
   */
  public LocalDateRangeValidate(LocalDate data, LocalDate minimum, LocalDate maximum, String printPattern) {  
    
    // Nincs adat ?
    if (data == null) {
      return;
    }
    
    // Validáció a minimum értékre
    if (minimum != null && data.isBefore(minimum)) {
      message = new Message(
        "xesj.spring.validation.LocalDateRangeValidate.minimum", 
        new String[]{minimum.format(DateTimeFormatter.ofPattern(printPattern))}, 
        null
      );
    }

    // Validáció a maximum értékre
    if (maximum != null && data.isAfter(maximum)) {
      message = new Message(
        "xesj.spring.validation.LocalDateRangeValidate.maximum", 
        new String[]{maximum.format(DateTimeFormatter.ofPattern(printPattern))}, 
        null
      );
    }

  }
  
  // ====
}
