package xesj.spring.validation.validate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import xesj.spring.validation.Message;
import xesj.tool.StringTool;

/**
 * Reguláris kifejezésnek megfelelőség ellenőrzése. 
 */
public class RegexpValidate extends Validate {
  
  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: ha van adat, van reguláris kifejezés, és az adat nem felel meg a reguláris kifejezésnek.<br/>
   * Érvényes: Ha nincs adat, vagy nincs reguláris kifejezés, vagy az adat megfelel a reguláris kifejezésnek.
   * @param data A vizsgálandó adat.
   * @param regexp Reguláris kifejezés, melynek meg kell felelnie az adatnak.
   */
  public RegexpValidate(String data, String regexp) {  
    
    // Van adat, reguláris kifejezés ?
    if (StringTool.isNullOrEmpty(data) || StringTool.isNullOrEmpty(regexp)) {
      return;
    }
    
    // Validáció
    Pattern pattern = Pattern.compile(regexp);
    Matcher matcher = pattern.matcher(data);
    if (matcher.matches()) {
      return;
    }
    else {
      message = new Message("xesj.spring.validation.RegexpValidate", null, null);
    }

  }
  
  // ====
}
