package xesj.spring.validation.validate;
import xesj.spring.validation.Message;

/**
 * Az adat kitöltöttségének ellenőrzése.
 * String típusú adat esetén az adat hosszának legalább 1 karakternek kell lennie, 
 * és a láthatósági validálás bekapcsolásakor tartalmaznia kell látható karaktert is.
 * Nem string típusú adat esetén az adat nem lehet null.
 */
public class RequiredValidate extends Validate {
  
  /**
   * Konstruktor, validáció.<br/>
   * Érvénytelen: 
   *   Ha az adat null, 
   *   vagy string típusú adat esetén üres string, 
   *   vagy a láthatóság validálás be van kapcsolva de a string típusú adat nem tartalmaz látható karaktert.<br/> 
   * Érvényes: egyéb esetben.
   * @param data A vizsgálandó adat.
   * @param visibleValidate A láthatóság validálás be legyen-e kapcsolva. Ha igen, akkor látható karaktert is 
   *                        tartalmaznia kell a vizsgálandó adatnak.
   */
  public RequiredValidate(Object data, boolean visibleValidate) {  

    if (data == null) {
      message = new Message("xesj.spring.validation.RequiredValidate.nodata", null, null);
      return;
    }
    if (data instanceof String) {
      if (data.toString().isEmpty()) {
        message = new Message("xesj.spring.validation.RequiredValidate.nodata", null, null);
        return;
      }
      if (visibleValidate) {
        if (data.toString().trim().isEmpty()) {
          message = new Message("xesj.spring.validation.RequiredValidate.novisible", null, null);
          return;
        }
      }
    }

  }

  /**
   * Konstruktor, validáció, a láthatóság validálás bekapcsolásával.<br/>
   * Érvénytelen: Ha az adat null, vagy a string típusú adat nem tartalmaz látható karaktert.<br/> 
   * Érvényes: egyéb esetben.
   * @param data A vizsgálandó adat.
   */
  public RequiredValidate(Object data) {  

    this(data, true);

  }
  
  // ====
}
