package xesj.spring.validation.convert;
import java.text.ParseException;
import lombok.NonNull;
import xesj.spring.validation.Message;
import xesj.tool.DateTool;
import xesj.tool.StringTool;

/**
 * Text --&gt; java.util.Date konverziót megvalósító osztály.
 */
public class DateConvert extends Convert {
  
  /**
   * Konstruktor, konverzió.
   * @param text A konvertálandó adat.
   * @param pattern A konvertálandó adat dátum formátuma.
   */
  public DateConvert(String text, @NonNull String pattern) {

    // Ha nincs konvertálandó adat, akkor a konverzió eredménye: null
    if (StringTool.isNullOrEmpty(text)) {
      return;
    }
    
    // Konverzió
    try {
      value = DateTool.parse(text, pattern);
    }
    catch (ParseException pe) {
      message = new Message("xesj.spring.validation.DateConvert", null, null);
    } 

  }
 
  // ====
}
