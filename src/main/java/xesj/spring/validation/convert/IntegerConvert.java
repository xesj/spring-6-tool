package xesj.spring.validation.convert;
import java.math.BigInteger;
import xesj.spring.validation.Message;
import xesj.tool.StringTool;

/**
 * Text --&gt; Integer konverziót megvalósító osztály.
 */
public class IntegerConvert extends Convert {
  
  /**
   * Konstruktor, konverzió.
   * @param text A konvertálandó adat.
   */
  public IntegerConvert(String text) {
    
    // Van adat ?
    if (StringTool.isNullOrEmpty(text)) {
      return;
    }
    
    // Konverzió
    try {
      value = Integer.valueOf(text);
    }
    catch (NumberFormatException nfe1) {
      // Nem lehet Integer-re konvertálni, de lehetséges hogy BigInteger-be lehet.
      try {
        new BigInteger(text);
        message = new Message(
          "xesj.spring.validation.IntegerConvert.interval", 
          new Integer[]{Integer.MIN_VALUE, Integer.MAX_VALUE}, 
          null
        );
      }
      catch (NumberFormatException nfe2) {
        message = new Message("xesj.spring.validation.IntegerConvert", null, null);
      }
    } 

  }
 
  // ====
}
