package xesj.spring.validation.convert;
import lombok.NonNull;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.ValidationUsageException;

/**
 * Konverzió ősosztály. 
 * Konverzió készítéskor ezt az osztályt kell leszármaztatni, és a leszármazott osztálynak csak a konstruktorát kell megírni.
 * A konstruktornak el kell végezni a konverziót.
 * Sikeres konverzió esetén az eredményt a "value" változóba kell tárolni.
 * Adathiba miatti sikertelen konverzió esetén a "message" változóba be kell állítani a hibaüzenetet, 
 * és a "value" változót nem kell kitölteni.
 */
public class Convert {
  
  /** Sikeres konverzió esetén a konverzió eredménye, különben null */
  protected Object value;

  /** Sikertelen konverzió esetén a hibaüzenet, különben null */
  protected Message message;
  
  /**
   * A konverzió során keletkezett hibaüzenet lekérdezése. Ha nem keletkezett hibaüzenet akkor null-t ad vissza.
   * @param msl Hibaüzeneteket visszafejtő objektum.
   * @return A hibaüzenet. Ha nem keletkezett hibaüzenet akkor null.
   */
  public String getMessage(@NonNull MessageSourceLocale msl) {

    return (message == null ? null : message.getMessageText(msl));

  }
  
  /**
   * A konverzió eredményének lekérdezése. 
   * Ezt a lekérdezést csak akkor szabad végrehajtani, ha a konverzió közben nem keletkezett hibaüzenet, 
   * különben ValidationUsageException keletkezik. 
   * A kapott eredményt CAST-olni szükséges a konverziótól függően. Példa:<br/><br/>
   * <pre>
   * LongConvert convert = new LongConvert(...);
   * if (convert.isValid()) {
   *   Long value = (Long)convert.getValue();
   * }
   * </pre>
   * @return A konverzió eredménye.
   */
  public Object getValue() {

    if (message != null) {
      throw new ValidationUsageException("Sikertelen konverzió esetén nem kérdezhető le a konverzió eredménye!");
    }
    return value;

  }
  
  /**
   * Konverzió sikerességének ellenőrzése.
   * @return True: sikeres konverzió, nem keletkezett hibaüzenet.<br/>
   *         False: sikertelen konverzió, keletkezett hibaüzenet.
   */
  public boolean isValid() {

    return (message == null);

  }

  // ====
}
