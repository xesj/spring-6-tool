package xesj.spring.validation.convert;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import lombok.NonNull;
import xesj.spring.validation.Message;
import xesj.tool.StringTool;

/**
 * Text --&gt; java.time.LocalDate konverziót megvalósító osztály.
 */
public class LocalDateConvert extends Convert {
  
  /**
   * Konstruktor, konverzió.
   * @param text A konvertálandó adat.
   * @param pattern A konvertálandó adat dátum formátuma, például: "uuuu.MM.dd"
   */
  public LocalDateConvert(String text, @NonNull String pattern) {

    // Ha nincs konvertálandó adat, akkor a konverzió eredménye: null
    if (StringTool.isNullOrEmpty(text)) {
      return;
    }
    
    // Konverzió
    try {
      value = LocalDate.parse(text, DateTimeFormatter.ofPattern(pattern).withResolverStyle(ResolverStyle.STRICT));
    }
    catch (DateTimeParseException pe) {
      message = new Message("xesj.spring.validation.LocalDateConvert", null, null);
    } 

  }
 
  // ====
}
