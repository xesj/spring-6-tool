package xesj.spring.property_editor;
import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.util.Date;
import lombok.NonNull;
import xesj.tool.DateTool;
import xesj.tool.StringTool;

/**
 * Text &lt;--&gt; java.util.Date konverziót megvalósító osztály.
 */
public class DatePropertyEditor extends PropertyEditorSupport {
  
  private String pattern;

  /**
   * Konstruktor
   * @param pattern Dátum formátum maszk, például: "yyyy.MM.dd"
   */
  public DatePropertyEditor(@NonNull String pattern) {

    this.pattern = pattern;

  }
  
  /**
   * Változóba konvertálás.
   */
  @Override
  public void setAsText(String text) throws IllegalArgumentException {

    if (StringTool.isNullOrEmpty(text)) {
      setValue(null);
      return;
    }
    try {
      Date date = DateTool.parse(text, pattern);
      setValue(date);
    }
    catch (ParseException pe) {
      throw new IllegalArgumentException();
    } 

  }
  
  /**
   * Képernyőre konvertálás.
   */
  @Override
  public String getAsText() {

    Date date = (Date)getValue();
    return (date == null ? "" : DateTool.format(date, pattern));

  }
  
  // ====
}
