package xesj.spring.property_editor;
import java.beans.PropertyEditorSupport;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import lombok.NonNull;
import xesj.tool.StringTool;

/**
 * Text &lt;--&gt; java.time.LocalDate konverziót megvalósító osztály.
 */
public class LocalDatePropertyEditor extends PropertyEditorSupport {
  
  private String pattern;

  /**
   * Konstruktor
   * @param pattern Dátum formátum maszk, például: "uuuu.MM.dd"
   */
  public LocalDatePropertyEditor(@NonNull String pattern) {

    this.pattern = pattern;

  }
  
  /**
   * Változóba konvertálás.
   */
  @Override
  public void setAsText(String text) throws IllegalArgumentException {

    if (StringTool.isNullOrEmpty(text)) {
      setValue(null);
      return;
    }
    try {
      LocalDate localDate = LocalDate.parse(
        text, 
        DateTimeFormatter.ofPattern(pattern).withResolverStyle(ResolverStyle.STRICT)
      );
      setValue(localDate);
    }
    catch (DateTimeParseException pe) {
      throw new IllegalArgumentException();
    } 

  }
  
  /**
   * Képernyőre konvertálás.
   */
  @Override
  public String getAsText() {

    LocalDate localDate = (LocalDate)getValue();
    return (localDate == null ? "" : localDate.format(DateTimeFormatter.ofPattern(pattern)));

  }
  
  // ====
}
