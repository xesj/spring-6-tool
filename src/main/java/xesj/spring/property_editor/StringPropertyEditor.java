package xesj.spring.property_editor;
import java.beans.PropertyEditorSupport;
import xesj.tool.StringTool;

/**
 * Text &lt;--&gt; String konverziót megvalósító osztály.<br/>
 * <br/>
 * Változóba konvertálás (text --&gt; String konverzió):<br/> 
 *   null -&gt; null, "" -&gt; null, "..." -&gt; "..."  
 * <br/><br/>
 * Képernyőre konvertálás (String --&gt; text konverzió):<br/> 
 *   null -&gt; "", "" -&gt; "", "..." -&gt; "..."  
 */
public class StringPropertyEditor extends PropertyEditorSupport {

  /**
   * Változóba konvertálás.
   * A text változatlan formába átrakható string-be, kivéve ha a text üres: "", mert ekkor null érték kerül a string-be.
   */
  @Override
  public void setAsText(String text) {

    if (StringTool.equal(text, "")) {
      setValue(null);
    }
    else {
      setValue(text);
    }

  }
  
  /**
   * Képernyőre konvertálás.
   * A string változatlan formába átrakható text-be, kivéve ha a string null, mert ekkor "" érték kerül a text-be.
   */
  @Override
  public String getAsText() {

    String str = (String)getValue();
    if (str == null) {
      return "";
    }
    else {
      return str;
    }

  }
  
  // ====
}
