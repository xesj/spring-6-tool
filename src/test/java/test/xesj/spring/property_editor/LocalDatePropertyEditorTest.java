package test.xesj.spring.property_editor;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import test.xesj.spring.TestConfiguration;
import xesj.spring.property_editor.LocalDatePropertyEditor;

/**
 * LocalDatePropertyEditor Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class LocalDatePropertyEditorTest {

  /**
   * setAsText() metódus teszt 
   */
  @Test
  public void setAsTextTest() throws ParseException {
    
    // Pattern = null
    assertThrows(
      NullPointerException.class,
      () -> { new LocalDatePropertyEditor(null); }
    );
    
    // Helyes adatok
    String[] pt = {"uuuu.MM.dd", "2018.11.22", "dd-MM-uuuu", "31-12-2008"};
    for (int i = 0; i < pt.length; i += 2) {
      LocalDatePropertyEditor editor = new LocalDatePropertyEditor(pt[i]);
      editor.setAsText(pt[i + 1]);
      assertEquals(
        LocalDate.parse(pt[i + 1], DateTimeFormatter.ofPattern(pt[i]).withResolverStyle(ResolverStyle.STRICT)), 
        (LocalDate)editor.getValue()
      );
    }
    
    // Hibás adatok, IllegalArgumentException-t várunk
    pt = new String[]{"uuuu.MM.dd", "2018.22.11", "dd-MM-uuuu", "29-02-2017"};
    for (int i = 0; i < pt.length; i += 2) {
      LocalDatePropertyEditor editor = new LocalDatePropertyEditor(pt[i]);
      String str = pt[i + 1];
      assertThrows(
        IllegalArgumentException.class,
        () -> { editor.setAsText(str); }
      );
    }

  }  
  
  /**
   * getAsText() metódus teszt 
   */
  @Test
  public void getAsTextTest() throws ParseException {

    String[] patterns = {"uuuu.MM.dd", "uuuu", "dd-MM-uuuu"};
    LocalDate localDate = LocalDate.now();
    for (String pattern: patterns) {
      LocalDatePropertyEditor editor = new LocalDatePropertyEditor(pattern);
      editor.setValue(localDate);
      assertEquals(
        localDate.format(DateTimeFormatter.ofPattern(pattern)), 
        editor.getAsText()
      );
    }

  }
  
  // ====
}
