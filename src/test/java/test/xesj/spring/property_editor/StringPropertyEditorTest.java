package test.xesj.spring.property_editor;
import java.text.ParseException;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import test.xesj.spring.TestConfiguration;
import xesj.spring.property_editor.StringPropertyEditor;

/**
 * StringPropertyEditor Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class StringPropertyEditorTest {

  /**
   * setAsText() metódus teszt 
   */
  @Test
  public void setAsTextTest() throws ParseException {

    String[] array = {null, "", " ", " abc ", "\r\n\t", "x y Z", "árvíztűrő TÜKÖRFÚRÓGÉP"};
    for (String str: array) {
      StringPropertyEditor editor = new StringPropertyEditor();
      editor.setAsText(str);
      if (str != null && str.isEmpty()) {
        // Üres string esetén a változóba null-t kell rakni
        assertNull(editor.getValue());
        
      }
      else {
        // Általános eset (nem üres string)
        assertEquals(str, (String)editor.getValue());
      }
    }

  }  
  
  /**
   * getAsText() metódus teszt 
   */
  @Test
  public void getAsTextTest() throws ParseException {

    String[] array = {null, "", " ", " abc ", "\r\n\t", "x y Z", "árvíztűrő TÜKÖRFÚRÓGÉP"};
    for (String str: array) {
      StringPropertyEditor editor = new StringPropertyEditor();
      editor.setValue(str);
      if (str == null) {
        // Null esetén a képernyőre ""-t kell írni
        assertEquals("", editor.getAsText());
      }
      else {
        // Általános eset (nem üres string)
        assertEquals(str, editor.getAsText());
      }
    }

  }
  
  // ====
}
