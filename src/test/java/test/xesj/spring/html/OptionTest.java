package test.xesj.spring.html;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.html.Option;

/**
 * Option teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class OptionTest {

  /**
   * Teszt 
   */
  @Test
  public void test() {

    Option option;
    
    // Null érték 1. eset
    option = new Option();
    assertEquals("", option.getDisplay());
    assertEquals("", option.getValue());

    // Null érték 2. eset
    option = new Option();
    option.setValue(null);
    option.setDisplay(null);
    assertEquals("", option.getDisplay());
    assertEquals("", option.getValue());

    // Null érték 3. eset
    option = new Option(null, null);
    assertEquals("", option.getDisplay());
    assertEquals("", option.getValue());
    
    // Egyéb érték
    option = new Option("v", "D");
    assertEquals("D", option.getDisplay());
    assertEquals("v", option.getValue());

  }  
  
  // ====
}
