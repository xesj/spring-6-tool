package test.xesj.spring.validation;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import test.xesj.spring.validation.validate.TestExceptionValidate;
import xesj.spring.validation.FormValidationContext;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.ValidationUsageException;
import xesj.spring.validation.validate.IntegerRangeValidate;
import xesj.tool.LocaleTool;
import xesj.tool.RandomTool;

/**
 * FormValidationContext Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class FormValidationContext_Test {

  private static final String OBJECT_NAME = "objectname";
  
  @Autowired MessageSource messageSource;

  /**
   * BindingResult visszaadása
   */
  private BindingResult getBindingResult() {

    FormValidationContextObject object = new FormValidationContextObject();
    BeanPropertyBindingResult result = new BeanPropertyBindingResult(object, OBJECT_NAME);

    // BindingResult konverziós hibák beállítása
    FieldError fieldError1 = new FieldError(OBJECT_NAME, "r1", "x123", true, null, null, "Nem egész szám");
    FieldError fieldError2 = new FieldError(OBJECT_NAME, "r2", "xy", true, null, null, "Nem egész szám");
    result.addError(fieldError1);
    result.addError(fieldError2);
    
    // Válasz
    return result;

  }

  /**
   * FormValidationContext visszaadása
   */
  private FormValidationContext getFormValidationContext(
    BindingResult result, 
    Integer fieldMessageMaximum, 
    Integer globalMessageMaximum) {
    
    // FormValidationContext
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    FormValidationContext context = new FormValidationContext(result, msl, fieldMessageMaximum, globalMessageMaximum);
    return context;

  }

  /**
   * Konstruktor teszt
   */
  @Test
  public void constructor_Test() {

    // Előkészítés
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // Helyes konstruktor hívások
    new FormValidationContext(getBindingResult(), msl, null, null);
    new FormValidationContext(
      getBindingResult(), 
      msl, 
      RandomTool.interval(1, 9999999), 
      RandomTool.interval(1, 9999999)
    );
    
    // A bindingResult paraméter nem lehet null
    assertThrows(
      NullPointerException.class,
      () -> new FormValidationContext(null, msl, 1, 1)
    );
    
    // Az msl paraméter nem lehet null
    assertThrows(
      NullPointerException.class,
      () -> new FormValidationContext(getBindingResult(), null, 1, 1)
    );
    
    // A fieldMessageMaximum paraméter nem lehet 1-nél kisebb
    assertThrows(
      ValidationUsageException.class,
      () -> new FormValidationContext(getBindingResult(), msl, RandomTool.interval(-99, 0), 1)
    );
    
    // A globalMessageMaximum paraméter nem lehet 1-nél kisebb
    assertThrows(
      ValidationUsageException.class,
      () -> new FormValidationContext(getBindingResult(), msl, 1, RandomTool.interval(-99, 0))
    );
    
  }
  
  /**
   * add(field, Supplier) teszt
   */
  @Test
  public void addFieldSupplier_Test() {

    // 2 hibaüzenet  
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, null, null);
      int data = 7;
      context.add("szam", 
        () -> new IntegerRangeValidate(data, 100, 200), // 1. hiba
        () -> new IntegerRangeValidate(data, 5, 7),
        () -> new IntegerRangeValidate(data, -5, 0)     // 2. hiba
      );
      assertEquals(2, result.getFieldErrorCount("szam"));
    }

    // 1 hibaüzenet: globalMessageMaximum = 1  
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, null, 1);
      int data = 7;
      context.add(null, 
        () -> new IntegerRangeValidate(data, 100, 200), // 1. hiba
        () -> new IntegerRangeValidate(data, 7, 7),
        () -> new IntegerRangeValidate(data, -5, 0),    // 2. hiba (nem jegyződik be)
        () -> new TestExceptionValidate("x")            // nem fut le
      );
      assertEquals(1, result.getGlobalErrorCount());
    }

    // 1 hibaüzenet, fieldMessageMaximum = 1  
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, 1, null);
      int data = 7;
      context.add("datum", 
        () -> new IntegerRangeValidate(data, 100, 200), // 1. hiba
        () -> new IntegerRangeValidate(data, 7, 7),
        () -> new IntegerRangeValidate(data, -5, 0),    // 2. hiba (nem jegyződik be)
        () -> new TestExceptionValidate("x")            // nem fut le
      );
      assertEquals(1, result.getFieldErrorCount("datum"));
    }
    
    // A validáció nem fut le konverziós hiba miatt  
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, null, null);
      context.add("r1", 
        () -> new TestExceptionValidate("x") // nem fut le
      );
      context.add("r2", 
        () -> new TestExceptionValidate("x") // nem fut le
      );
    }
    
  }
  
  /**
   * add(field, Message) teszt
   */
  @Test
  public void addFieldMessage_Test() {

    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, null, null);
      context.add(null, new Message("test.error", new String[]{"ABC"}, null));
      assertEquals("Teszt hiba: ABC", result.getGlobalErrors().get(0).getDefaultMessage());
    }
    
    // Üres hibaüzenet exception-t vált ki
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, null, null);
      assertThrows(
        NullPointerException.class,
        () -> context.add("m1", (Message)null)
      );
    }
  
  }
  
  /**
   * add(field, String) teszt
   */
  @Test
  public void addFieldString_Test() {

    // fieldMessageMaximum = 1, globalMessageMaximum = 1
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, 1, 1);

      context.add("szam", "árvíztűrő");
      context.add(null, "gepárd");
      
      assertEquals(1, result.getGlobalErrorCount());
      assertEquals(1, result.getFieldErrorCount("szam"));
      assertEquals("gepárd", result.getGlobalErrors().get(0).getDefaultMessage());
      assertEquals("árvíztűrő", result.getFieldErrors("szam").get(0).getDefaultMessage());

      context.add("szam", "nem kerül be"); // nem kerül be
      context.add(null, "nem kerül be");   // nem kerül be

      assertEquals(1, result.getGlobalErrorCount());
      assertEquals(1, result.getFieldErrorCount("szam"));
    }
    
    // Üres üzenetek hibát váltanak ki
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, 1, 1);

      assertThrows(
        NullPointerException.class,
        () -> context.add(null, (String)null)
      );

      assertThrows(
        ValidationUsageException.class,
        () -> context.add("m1", "")
      );
    }
  
  }
  
  /**
   * getBindingResult() teszt
   */
  @Test
  public void getBindingResult_Test() {

    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, 1, 1);
      assertTrue(result == context.getBindingResult());
    }

  }  
  
  /**
   * enableAddMessage() teszt
   */
  @Test
  public void enableAddMessage_Test() {

    // fieldMessageMaximum = 2
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, 2, null);

      assertTrue(context.enableAddMessage("szam"));
      assertTrue(context.enableAddMessage(null));
      assertFalse(context.enableAddMessage("r1")); // konverziós hiba a mezőn
      assertFalse(context.enableAddMessage("r2")); // konverziós hiba a mezőn

      context.add("szam", () -> new IntegerRangeValidate(99, 1, 2)); // szam: 1. hibaüzenet
      assertTrue(context.enableAddMessage("szam"));
      assertTrue(context.enableAddMessage(null));
      
      context.add("szam", () -> new IntegerRangeValidate(77, 1, 2)); // szam: 2. hibaüzenet (maximum)
      context.add(null, () -> new IntegerRangeValidate(99, 1, 2));   // 1. globális hibaüzenet 
      context.add(null, () -> new IntegerRangeValidate(88, 1, 2));   // 2. globális hibaüzenet
      context.add(null, () -> new IntegerRangeValidate(77, 1, 2));   // 3. globális hibaüzenet
      assertFalse(context.enableAddMessage("szam"));
      assertTrue(context.enableAddMessage(null));
    }
    
    // globalMessageMaximum = 2
    {
      BindingResult result = getBindingResult();
      FormValidationContext context = getFormValidationContext(result, null, 2);

      assertFalse(context.enableAddMessage("r1")); // konverziós hiba a mezőn
      assertFalse(context.enableAddMessage("r2")); // konverziós hiba a mezőn
      assertTrue(context.enableAddMessage("szam"));
      assertTrue(context.enableAddMessage(null));

      context.add("szam", () -> new IntegerRangeValidate(34, 1, 2)); // szam: 1. hibaüzenet
      context.add("szam", () -> new IntegerRangeValidate(35, 1, 2)); // szam: 2. hibaüzenet
      context.add("szam", "hiba3");                                  // szam: 3. hibaüzenet
      assertTrue(context.enableAddMessage("szam"));
      assertTrue(context.enableAddMessage(null));
      
      context.add(null, () -> new IntegerRangeValidate(34, 1, 2));   // 1. globális hibaüzenet
      assertTrue(context.enableAddMessage("szam"));
      assertTrue(context.enableAddMessage(null));
      
      context.add(null, "hiba2");                                    // 2. globális hibaüzenet (maximum)
      assertTrue(context.enableAddMessage("szam"));
      assertFalse(context.enableAddMessage(null));
    }
    
  }
  
  /**
   * getFieldMessageMaximum() teszt
   */
  @Test
  public void getFieldMessageMaximum_Test() {
    
    { 
      FormValidationContext context = getFormValidationContext(getBindingResult(), null, 5);
      assertNull(context.getFieldMessageMaximum());
    }  

    {
      FormValidationContext context = getFormValidationContext(getBindingResult(), 5, 10);
      assertEquals(5, context.getFieldMessageMaximum());
    }  

  }  
  
  /**
   * getGlobalMessageMaximum() teszt
   */
  @Test
  public void getGlobalMessageMaximum_Test() {
    
    { 
      FormValidationContext context = getFormValidationContext(getBindingResult(), 5, null);
      assertNull(context.getGlobalMessageMaximum());
    }  

    {
      FormValidationContext context = getFormValidationContext(getBindingResult(), 5, 10);
      assertEquals(10, context.getGlobalMessageMaximum());
    }  

  }  
  
  // ====
}
