package test.xesj.spring.validation;
import java.awt.Color;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.*;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import test.xesj.spring.validation.validate.TestExceptionValidate;
import xesj.spring.validation.ConvertError;
import xesj.spring.validation.Message;
import xesj.spring.validation.ValidationContext;
import xesj.spring.validation.convert.LongConvert;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.ValidationContextException;
import xesj.spring.validation.ValidationUsageException;
import xesj.spring.validation.convert.Convert;
import xesj.spring.validation.convert.IntegerConvert;
import xesj.spring.validation.validate.IntegerRangeValidate;
import xesj.spring.validation.validate.LongRangeValidate;
import xesj.tool.LocaleTool;
import xesj.tool.RandomTool;

/**
 * ValidationContext Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class ValidationContext_Test {
  
  @Autowired MessageSource messageSource;
  
  /**
   * Validation Context készítő
   */
  private ValidationContext getValidationContext(
    Integer fieldMessageMaximum, Integer globalMessageMaximum, Integer throwCount) {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    return new ValidationContext(msl, fieldMessageMaximum, globalMessageMaximum, throwCount);
    
  } 

  /**
   * Konstruktor teszt
   */
  @Test
  public void constructor_Test() {

    // Előkészítés
    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // Helyes konstruktor hívások
    new ValidationContext(msl, null, null, null);
    new ValidationContext(msl, 
      RandomTool.interval(1, 9999999), 
      RandomTool.interval(1, 9999999), 
      RandomTool.interval(1, 9999999)
    );
    
    // Az msl paraméter nem lehet null
    assertThrows(
      NullPointerException.class,
      () -> new ValidationContext(null, 1, 1, 1)
    );
    
    // A throwCount paraméter nem lehet 1-nél kisebb
    assertThrows(
      ValidationUsageException.class,
      () -> new ValidationContext(msl, null, null, RandomTool.interval(-99, 0))
    );
    
    // A fieldMessageMaximum paraméter nem lehet 1-nél kisebb
    assertThrows(
      ValidationUsageException.class,
      () -> new ValidationContext(msl, RandomTool.interval(-99, 0), null, null)
    );
    
    // A globalMessageMaximum paraméter nem lehet 1-nél kisebb
    assertThrows(
      ValidationUsageException.class,
      () -> new ValidationContext(msl, null, RandomTool.interval(-99, 0), null)
    );
    
  }

  /**
   * add(field, convert) teszt
   */
  @Test
  public void addFieldConvert_Test() {

    // Előkészítés
    ValidationContext context = getValidationContext(1, 1, null);
    
    // Helyes konverzió
    context.add(null, new LongConvert("123"));
    assertTrue(context.hasConvertValue(null, true));
    assertEquals(context.getConvertValue(null), 123L);

    // Hibás konverzió
    context.add("m1", new LongConvert("rossz"));
    assertTrue(context.hasConvertValue("m1", false));
    assertFalse(context.hasConvertValue("m1", true));
    assertTrue(context.getConvertValue("m1") instanceof ConvertError);

    // Nincs megadva convert
    assertThrows(
      NullPointerException.class,
      () -> context.add(null, (Convert)null)
    );  

    // Teszt: dupla konverzió a null mezőn
    assertThrows(
      ValidationUsageException.class,
      () -> { 
        context.add(null, new LongConvert("7")); 
        context.add(null, new LongConvert("8")); 
      }
    );  

    // Teszt: dupla konverzió az m2 mezőn
    assertThrows(
      ValidationUsageException.class,
      () -> { 
        context.add("m2", new LongConvert("7")); 
        context.add("m2", new LongConvert("8")); 
      }
    );  
    
    // Teszt: már van hibaüzenet a mezőn
    context.add("m3", "globális hiba");
    assertThrows(
      ValidationUsageException.class,
      () -> { 
        context.add("m3", new LongConvert("0")); 
      }
    );  

  }
  
  /**
   * add(field, convert, Function) teszt
   */
  @Test
  public void addFieldConvertFunction_Test() {
    
    // FieldMessageMaximum = 1
    {
      ValidationContext context = getValidationContext(1, null, null);
      context.add(
        "m1", 
        new LongConvert("23"),
        (c) -> new LongRangeValidate((Long)c, 100L, 200L), // 1. hiba
        (c) -> new LongRangeValidate((Long)c, 10L, 70L),
        (c) -> new LongRangeValidate((Long)c, -5L, 0L)     // 2. hiba (nem jegyződik be)
      );
      context.add(
        null, 
        new LongConvert("56"),
        (c) -> new LongRangeValidate((Long)c, 100L, 200L), // 1. hiba
        (c) -> new LongRangeValidate((Long)c, 10L, 70L),
        (c) -> new LongRangeValidate((Long)c, -5L, 0L)     // 2. hiba
      );
      assertEquals(1, context.getMessages("m1").size());
      assertEquals(2, context.getMessages(null).size());
    }  

    // GlobalMessageMaximum = 1
    {
      ValidationContext context = getValidationContext(null, 1, null);
      context.add(
        "m1", 
        new LongConvert("40"),
        (c) -> new LongRangeValidate((Long)c, 100L, 200L), // 1. hiba
        (c) -> new LongRangeValidate((Long)c, 10L, 70L),
        (c) -> new LongRangeValidate((Long)c, -5L, 0L)     // 2. hiba
      );
      context.add(
        null, 
        new LongConvert("38"),
        (c) -> new LongRangeValidate((Long)c, 100L, 200L), // 1. hiba
        (c) -> new LongRangeValidate((Long)c, 10L, 70L),
        (c) -> new LongRangeValidate((Long)c, -5L, 0L)     // 2. hiba (nem jegyződik be)
      );
      assertEquals(2, context.getMessages("m1").size());
      assertEquals(1, context.getMessages(null).size());
    }    
    
    // A validáció nem futhat le, mert a konverzió sikertelen volt
    {
      ValidationContext context = getValidationContext(null, null, null);
      context.add(
        "m1", 
        new LongConvert("xx"),
        (c) -> new TestExceptionValidate(c.toString()),              // nem fut le
        (c) -> new TestExceptionValidate(c.toString().toLowerCase()) // nem fut le
      );
      assertEquals(1, context.getMessages("m1").size());
    } 
    
    // A 3. hibánál ValidationContextException keletkezik
    {
      ValidationContext context = getValidationContext(null, null, 3);
      String adat = "23";
      assertThrows(
        ValidationContextException.class,
        () -> {
          context.add(
            null, 
            new LongConvert(adat),
            (c) -> new LongRangeValidate((Long)c, 100L, 200L), // 1. hiba
            (c) -> new LongRangeValidate((Long)c, 10L, 70L),
            (c) -> new LongRangeValidate((Long)c, -5L, 0L),    // 2. hiba
            (c) -> new LongRangeValidate((Long)c, 3L, 4L)      // 3. hiba
          );
        }  
      );
    }  
  
  }

  /**
   * add(field, Function) teszt
   */
  @Test
  public void addFieldFunction_Test() {
    
    // Normális eset
    {
      ValidationContext context = getValidationContext(null, null, null);
      context.add("m1", new LongConvert("23"));
      context.add(
        "m1", 
        (c) -> new LongRangeValidate((Long)c, 100L, 200L), // 1. hiba
        (c) -> new LongRangeValidate((Long)c, 10L, 70L),
        (c) -> new LongRangeValidate((Long)c, -5L, 0L)     // 2. hiba
      );
      assertEquals(2, context.getMessages("m1").size());
    }
    
    // Ha nincs előtte konverzió, akkor nem használható a Functional függvény
    {
      ValidationContext context = getValidationContext(null, null, null);
      assertThrows(
        ValidationUsageException.class,
        () -> {
          context.add(
            null, 
            (c) -> new LongRangeValidate((Long)c, 100L, 200L)
          );
        }  
      );
      assertThrows(
        ValidationUsageException.class,
        () -> {
          context.add(
            "m1", 
            (c) -> new LongRangeValidate((Long)c, 100L, 200L)
          );
        }  
      );
    }
    
    // A második validáció nem is futhat le, mert a FieldMessageMaximum = 1
    {
      ValidationContext context = getValidationContext(1, 1, null);
      context.add("m1", new IntegerConvert("1"));
      context.add(
        "m1", 
        (c) -> new IntegerRangeValidate((Integer)c, 2, 3),
        (c) -> new TestExceptionValidate("x")    // nem futhat le
      );
      assertEquals(1, context.getMessages("m1").size());
    } 

    // A validáció nem is futhat le, mert konverziós hiba volt
    {
      ValidationContext context = getValidationContext(null, null, null);
      context.add("m1", new IntegerConvert("rossz"));
      context.add(
        "m1", 
        (c) -> new TestExceptionValidate("x")    // nem futhat le
      );
      assertEquals(1, context.getMessages("m1").size());
    } 
    
  } 
  
  /**
   * add(field, Supplier) teszt
   */
  @Test
  public void addFieldSupplier_Test() {
    
    // Nem lehet előtte konverzió
    {
      ValidationContext context = getValidationContext(null, null, null);
      context.add(null, new LongConvert("rossz"));
      assertThrows(
        ValidationUsageException.class,
        () -> {
          context.add(
            null, 
            () -> new LongRangeValidate(150L, 100L, 200L)
          );
        }  
      );
    }

    // 2 hibaüzenet  
    {
      ValidationContext context = getValidationContext(null, null, null);
      long data = 23L;
      context.add(
        "m1", 
        () -> new LongRangeValidate(data, 100L, 200L), // 1. hiba
        () -> new LongRangeValidate(data, 10L, 70L),
        () -> new LongRangeValidate(data, -5L, 0L)     // 2. hiba
      );
      assertEquals(2, context.getMessages("m1").size());
    }

    // 1 hibaüzenet, mert a globalMessageMaximum = 1  
    {
      ValidationContext context = getValidationContext(null, 1, null);
      long data = 23L;
      context.add(
        null, 
        () -> new LongRangeValidate(data, 100L, 200L), // 1. hiba
        () -> new LongRangeValidate(data, 10L, 70L),
        () -> new LongRangeValidate(data, -5L, 0L)     // 2. hiba (nem jegyződik be)
      );
      assertEquals(1, context.getMessages(null).size());
    }
    
    // A validáció nem futhat le, mert fieldMessageMaximum = 1
    {
      ValidationContext context = getValidationContext(1, 99, null);
      context.add(
        "m1", 
        () -> new IntegerRangeValidate(1, 2, 3),
        () -> new TestExceptionValidate("x")    // nem fut le
      );
    } 
    
  }

  /**
   * add(field, Message) teszt
   */
  @Test
  public void addFieldMessage_Test() {

    {
      ValidationContext context = getValidationContext(1, 1, null);
      context.add(
        null, 
        new Message("test.error", new String[]{"ABC"}, null)
      );
      assertEquals("Teszt hiba: ABC", context.getMessages(null).get(0));
    }
    
    // Üres hibaüzenet exception-t vált ki
    {
      ValidationContext context = getValidationContext(1, 1, null);
      assertThrows(
        NullPointerException.class,
        () -> context.add("m1", (Message)null)
      );
    }
  
  }
  
  /**
   * add(field, String) teszt
   */
  @Test
  public void addFieldString_Test() {

    // Normál eset
    {
      ValidationContext context = getValidationContext(1, 1, null);
      context.add("m1", "árvíztűrő");
      context.add(null, "gepárd");
      
      assertEquals("gepárd", context.getMessages(null).get(0));
      assertEquals("árvíztűrő", context.getMessages("m1").get(0));

      context.add("m1", "nem kerül be");
      context.add(null, "nem kerül be");
      context.add("m2", new IntegerConvert("rossz"));
      context.add("m2", "nem kerül be");
      
      assertEquals(1, context.getMessages(null).size());
      assertEquals(1, context.getMessages("m1").size());
      assertEquals(1, context.getMessages("m2").size());
      assertEquals(0, context.getMessages("m3").size());
    }

    // Üres üzenetek hibát váltanak ki
    {
      ValidationContext context = getValidationContext(null, null, null);

      assertThrows(
        NullPointerException.class,
        () -> context.add(null, (String)null)
      );

      assertThrows(
        ValidationUsageException.class,
        () -> context.add("m1", "")
      );
  
    }

    // "" mezőnév hibát vált ki
    {
      ValidationContext context = getValidationContext(null, null, null);
      assertThrows(
        ValidationUsageException.class,
        () -> context.add("", "abc")
      );
    }
    
  }

  /**
   * getMarker(), setMarker() teszt
   */
  @Test
  public void getMarker_setMarker_Test() {
  
    {
      ValidationContext context = getValidationContext(null, null, null);
      assertNull(context.getMarker());
      context.setMarker("tűrő");
      context.add("a", "b");
      assertEquals("tűrő", context.getMarker().toString());
      
      context.setMarker(null);
      assertNull(context.getMarker());
      
      context.setMarker(Color.MAGENTA);
      assertEquals(Color.MAGENTA, context.getMarker());
    }
    
  }
  
  /**
   * getConvertValue() teszt
   */
  @Test
  public void getConvertValue_Test() {

    {
      ValidationContext context = getValidationContext(1, 1, null);
      context.add("m1", new LongConvert("xx"));
      context.add(null, new LongConvert("89"));
      
      assertTrue(context.getConvertValue("m1") instanceof ConvertError);
      assertEquals(context.getConvertValue(null), 89L);
      assertThrows(
        ValidationUsageException.class,
        () -> context.getConvertValue("m2")
      );
    }
    
  }  
  
  /**
   * getMessages() teszt
   */
  @Test
  public void getMessages_Test() {

    {
      ValidationContext context = getValidationContext(null, 3, null);
      assertTrue(context.getMessages("m1").isEmpty());
      assertTrue(context.getMessages(null).isEmpty());
      
      context.add("m1", "első");
      context.add(null, "x");
      context.add("m99", "kilenc1");
      context.add(null, "y");
      context.add("m1", "második");
      context.add("m99", "kilenc2");
      context.add(null, "z");
      context.add(null, "w"); // nem kerül be
      
      assertEquals(2, context.getMessages("m1").size());
      assertEquals("első", context.getMessages("m1").get(0));
      assertEquals("második", context.getMessages("m1").get(1));
      assertEquals(2, context.getMessages("m99").size());
      assertEquals("kilenc1", context.getMessages("m99").get(0));
      assertEquals("kilenc2", context.getMessages("m99").get(1));
      assertEquals(3, context.getMessages(null).size());
      assertEquals("x", context.getMessages(null).get(0));
      assertEquals("y", context.getMessages(null).get(1));
      assertEquals("z", context.getMessages(null).get(2));
    }    
    
  }

  /**
   * getFields() teszt
   */
  @Test
  public void getFields_Test() {

    {
      ValidationContext context = getValidationContext(null, null, null);

      assertTrue(context.getFields(false).isEmpty());
      assertTrue(context.getFields(true).isEmpty());

      context.add("m1", "hiba");
      context.add(null, "hiba");
      context.add(null, "hiba");
      context.add("m1", "hiba");
      context.add("m2", "hiba");

      assertEquals(2, context.getFields(false).size());
      assertEquals("m1", context.getFields(false).get(0));
      assertEquals("m2", context.getFields(false).get(1));

      assertEquals(3, context.getFields(true).size());
      assertEquals("m1", context.getFields(true).get(0));
      assertEquals(null, context.getFields(true).get(1));
      assertEquals("m2", context.getFields(true).get(2));
    }
    
  }

  /**
   * isFlag(), setFlag() teszt
   */
  @Test
  public void isFlag_setFlag_Test() {
  
    {
      ValidationContext context = getValidationContext(null, 1, null);
      assertTrue(context.isFlag());

      context.add(null, "hiba");
      assertFalse(context.isFlag());

      context.setFlag(true);
      assertTrue(context.isFlag());

      context.add(null, "hiba"); // nem kerül be
      assertTrue(context.isFlag());
    }
    
  }  
  
  /**
   * hasConvertValue() teszt
   */
  @Test
  public void hasConvertValue_Test() {

    {
      ValidationContext context = getValidationContext(1, 1, null);

      assertFalse(context.hasConvertValue(null, false));
      assertFalse(context.hasConvertValue(null, true));
      assertFalse(context.hasConvertValue("m1", false));
      assertFalse(context.hasConvertValue("m1", true));

      context.add("m1", new LongConvert("rossz"));
      context.add(null, new LongConvert("8"));

      assertTrue(context.hasConvertValue("m1", false));
      assertFalse(context.hasConvertValue("m1", true));
      
      assertTrue(context.hasConvertValue(null, false));
      assertTrue(context.hasConvertValue(null, true));

      assertFalse(context.hasConvertValue("m2", false));
      assertFalse(context.hasConvertValue("m2", true));
    }
    
  }  

  /**
   * hasFieldMessage(), hasFieldMessage(field) teszt
   */
  @Test
  public void hasFieldMessage_Test() {

    {
      ValidationContext context = getValidationContext(1, 1, null);
      assertFalse(context.hasFieldMessage());
      assertFalse(context.hasFieldMessage("m2"));

      context.add("m1", new LongConvert("1"));
      context.add("m2", new LongConvert("xx"));
      context.add(null, new LongConvert("yy"));

      assertTrue(context.hasFieldMessage());
      assertFalse(context.hasFieldMessage("m1"));
      assertTrue(context.hasFieldMessage("m2"));
    }
    
    // Nem lehet a mezőnév null
    {
      ValidationContext context = getValidationContext(null, null, null);
      assertThrows(
        NullPointerException.class,
        () -> context.hasFieldMessage(null)
      );
    }
    
  }  

  /**
   * hasGlobalMessage() teszt
   */
  @Test
  public void hasGlobalMessage_Test() {

    {
      ValidationContext context = getValidationContext(1, 1, null);
      assertFalse(context.hasGlobalMessage());

      context.add("m1", "hiba");
      assertFalse(context.hasGlobalMessage());

      context.add(null, new LongConvert("rossz"));
      assertTrue(context.hasGlobalMessage());
    }
    
  }  

  /**
   * hasMessage() teszt
   */
  @Test
  public void hasMessage_Test() {

    // Mezőhöz tartozó hibaüzenet kerül a kontextusba
    {
      ValidationContext context = getValidationContext(1, 1, null);
      assertFalse(context.hasMessage());

      context.add("m1", "hiba");
      assertTrue(context.hasMessage());
    }

    // Globális hibaüzenet kerül a kontextusba
    {
      ValidationContext context = getValidationContext(1, 1, null);
      assertFalse(context.hasMessage());

      context.add(null, "hiba");
      assertTrue(context.hasMessage());
    }
    
  }  

  /**
   * throwExceptionIfDirty() teszt
   */
  @Test
  public void throwExceptionIfDirty_Test() {

    // Globális hibaüzenet kerül a kontextusba
    {
      ValidationContext context = getValidationContext(1, 1, null);
      context.throwExceptionIfDirty();
      
      context.add(null, "hiba");
      assertThrows(
        ValidationContextException.class,
        () -> context.throwExceptionIfDirty()
      );
    }    

    // Mezőhöz tartozó hibaüzenet kerül a kontextusba
    {
      ValidationContext context = getValidationContext(1, 1, null);
      context.throwExceptionIfDirty();
      
      context.add("m1", "hiba");
      assertThrows(
        ValidationContextException.class,
        () -> context.throwExceptionIfDirty()
      );
    }    
    
  }

  /**
   * enableAddMessage() teszt
   */
  @Test
  public void enableAddMessage_Test() {

    // fieldMessageMaximum = 2
    {
      ValidationContext context = getValidationContext(2, null, null);
      assertTrue(context.enableAddMessage("m1"));
      assertTrue(context.enableAddMessage(null));

      context.add("m1", new LongConvert("xx"));     // m1: 1. üzenet
      assertFalse(context.enableAddMessage("m1"));
      context.add("m2", "hiba1");                   // m2: 1. üzenet
      assertTrue(context.enableAddMessage("m2"));
      context.add("m2", "hiba2");                   // m2: 2. üzenet
      assertFalse(context.enableAddMessage("m2"));
      assertTrue(context.enableAddMessage(null));
      
      context.add(null, new LongConvert("xx"));
      assertFalse(context.enableAddMessage("m1"));
      assertFalse(context.enableAddMessage(null));
    }
    
    // globalMessageMaximum = 1
    {
      ValidationContext context = getValidationContext(null, 1, null);
      assertTrue(context.enableAddMessage("m1"));
      assertTrue(context.enableAddMessage(null));

      context.add("m1", () -> new IntegerRangeValidate(34, 1, 2));
      assertTrue(context.enableAddMessage("m1"));
      assertTrue(context.enableAddMessage(null));
      
      context.add(null, () -> new IntegerRangeValidate(34, 1, 2));
      assertTrue(context.enableAddMessage("m1"));
      assertFalse(context.enableAddMessage(null));
    }
    
  }
  
  /**
   * toString() teszt
   */
  @Test
  public void toString_Test() {
    
    {
      ValidationContext context = getValidationContext(null, null, null);

      context.add(null, "global első hiba");
      context.add("m2", "m2 első hiba");
      context.add("m1", "m1 első hiba");
      context.add("m2", "m2 második hiba");
      context.add("m1", "m1 második hiba");
      context.add(null, "global második hiba");
      context.add("m1", "m1 harmadik hiba");
      context.add("m2", "m2 harmadik hiba");
      
      String expected =
        "m2: m2 első hiba\n" +
        "m2: m2 második hiba\n" +
        "m2: m2 harmadik hiba\n" +
        "m1: m1 első hiba\n" +
        "m1: m1 második hiba\n" +
        "m1: m1 harmadik hiba\n" +
        "global első hiba\n" +
        "global második hiba";

      assertEquals(expected, context.toString());
    }
    
  }
  
  /**
   * checkField() teszt
   */
  @Test
  public void checkField_Test() {
    
    // Helyes értékek
    ValidationContext.checkField(null);
    ValidationContext.checkField(" ");
    ValidationContext.checkField(".");
    assertThrows(
      ValidationUsageException.class,
      () -> ValidationContext.checkField("")
    );

    // Hibás érték
    assertThrows(
      ValidationUsageException.class,
      () -> ValidationContext.checkField("")
    );
    
  }  

  /**
   * checkMessage() teszt
   */
  @Test
  public void checkMessage_Test() {
    
    // Helyes értékek
    ValidationContext.checkMessage(" ");
    ValidationContext.checkMessage(".");

    // Hibás értékek
    assertThrows(
      ValidationUsageException.class,
      () -> ValidationContext.checkMessage(null)
    );
    assertThrows(
      ValidationUsageException.class,
      () -> ValidationContext.checkMessage("")
    );
    
  }

  /**
   * getFieldMessageMaximum() teszt
   */
  @Test
  public void getFieldMessageMaximum_Test() {
    
    { 
      ValidationContext context = getValidationContext(null, 10, 20);
      assertNull(context.getFieldMessageMaximum());
    }  

    {
      ValidationContext context = getValidationContext(5, 10, 20);
      assertEquals(5, context.getFieldMessageMaximum());
    }  

  }  
  
  /**
   * getGlobalMessageMaximum() teszt
   */
  @Test
  public void getGlobalMessageMaximum_Test() {
    
    { 
      ValidationContext context = getValidationContext(5, null, 20);
      assertNull(context.getGlobalMessageMaximum());
    }  

    {
      ValidationContext context = getValidationContext(5, 10, 20);
      assertEquals(10, context.getGlobalMessageMaximum());
    }  

  }  
  
  /**
   * getThrowCount() teszt
   */
  @Test
  public void getThrowCount_Test() {
    
    { 
      ValidationContext context = getValidationContext(5, 10, null);
      assertNull(context.getThrowCount());
    }  

    {
      ValidationContext context = getValidationContext(5, 10, 20);
      assertEquals(20, context.getThrowCount());
    }  

  }  
  
  
  // ====
}
