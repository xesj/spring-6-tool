package test.xesj.spring.validation.validate;
import xesj.spring.validation.Message;
import xesj.spring.validation.validate.Validate;

/**
 * Teszt validate
 */
public class TestValidate extends Validate {
  
  /**
   * Konstruktor
   */
  public TestValidate(String data) {

    message = new Message("test.error", new String[]{data}, null);

  }
  
  // ====
}
