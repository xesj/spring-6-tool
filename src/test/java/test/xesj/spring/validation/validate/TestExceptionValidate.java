package test.xesj.spring.validation.validate;
import xesj.spring.validation.validate.Validate;

/**
 * Teszt validate: ha fut, akkor mindig exception-t dob 
 */
public class TestExceptionValidate extends Validate {
  
  /**
   * Konstruktor
   */
  public TestExceptionValidate(String data) {

    throw new RuntimeException(data);
    
  }
  
  // ====
}
