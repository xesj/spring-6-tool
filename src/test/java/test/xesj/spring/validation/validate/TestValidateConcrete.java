package test.xesj.spring.validation.validate;
import xesj.spring.validation.Message;
import xesj.spring.validation.validate.Validate;

/**
 * Teszt validate concrete
 */
public class TestValidateConcrete extends Validate {
  
  /**
   * Konstruktor
   */
  public TestValidateConcrete(String data) {

    message = new Message(null, null, data);

  }
  
  // ====
}
