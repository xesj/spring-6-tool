package test.xesj.spring.validation.validate;
import java.time.LocalDate;
import test.xesj.spring.TestConfiguration;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.validate.LocalDateRangeValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * LocalDateRangeValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class LocalDateRangeValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    LocalDate now = LocalDate.now();    // most
    LocalDate past = now.minusDays(1);  // tegnap
    LocalDate future = now.plusDays(1); // holnap

    // Nincs adat    
    assertNull(new LocalDateRangeValidate(null, now, now, "uuuu.MM.dd").getMessage(msl));
    assertNull(new LocalDateRangeValidate(null, null, now, "uuuu.MM.dd").getMessage(msl));
    assertNull(new LocalDateRangeValidate(null, now, null, "uuuu.MM.dd").getMessage(msl));
    assertNull(new LocalDateRangeValidate(null, null, null, "uuuu.MM.dd").getMessage(msl));

    // Helyes az adat
    assertNull(new LocalDateRangeValidate(now, now, now, "uuuu.MM.dd").getMessage(msl));
    assertNull(new LocalDateRangeValidate(now, past, future, "uuuu").getMessage(msl));
    assertNull(new LocalDateRangeValidate(now, null, future, "uuuu").getMessage(msl));
    assertNull(new LocalDateRangeValidate(now, null, now, "uuuu").getMessage(msl));
    assertNull(new LocalDateRangeValidate(now, now, null, "uuuu").getMessage(msl));
    assertNull(new LocalDateRangeValidate(now, now, future, "uuuu").getMessage(msl));
    assertNull(new LocalDateRangeValidate(future, future, future, "uuuu").getMessage(msl));
    assertNull(new LocalDateRangeValidate(past, past, past, "uuuu").getMessage(msl));

    // Hibás az adat
    assertNotNull(new LocalDateRangeValidate(now, future, past, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(now, future, future, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(now, past, past, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(now, future, null, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(now, null, past, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(past, now, now, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(past, now, null, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(future, null, now, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(future, past, now, "uuuu").getMessage(msl));
    assertNotNull(new LocalDateRangeValidate(future, now, past, "uuuu").getMessage(msl));

  }
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    LocalDate now = LocalDate.now();    // most
    LocalDate past = now.minusDays(1);  // tegnap
    LocalDate future = now.plusDays(1); // holnap

    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new LocalDateRangeValidate(now, future, null, "uuuu.MM.dd").getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new LocalDateRangeValidate(now, null, past, "uuuu.MM.dd").getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
