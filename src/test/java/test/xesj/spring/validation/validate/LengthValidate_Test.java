package test.xesj.spring.validation.validate;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.validate.LengthValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * LengthValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class LengthValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat    
    assertNull(new LengthValidate(null, 5L, 3L).getMessage(msl));
    assertNull(new LengthValidate(null, null, 3L).getMessage(msl));
    assertNull(new LengthValidate(null, 3L, null).getMessage(msl));
    assertNull(new LengthValidate(null, null, null).getMessage(msl));
    assertNull(new LengthValidate("", 5L, 3L).getMessage(msl));
    assertNull(new LengthValidate("", null, 3L).getMessage(msl));
    assertNull(new LengthValidate("", 3L, null).getMessage(msl));
    assertNull(new LengthValidate("", null, null).getMessage(msl));

    // Helyes az adat
    assertNull(new LengthValidate(" ", 1L, 1L).getMessage(msl));
    assertNull(new LengthValidate(" ", null, 1L).getMessage(msl));
    assertNull(new LengthValidate(" ", null, 7L).getMessage(msl));
    assertNull(new LengthValidate(" ", 1L, 7L).getMessage(msl));
    assertNull(new LengthValidate("xyz", 2L, 7L).getMessage(msl));
    assertNull(new LengthValidate("xyz", 3L, 3L).getMessage(msl));
    assertNull(new LengthValidate("AB", null, 2L).getMessage(msl));
    assertNull(new LengthValidate("AB", null, 29L).getMessage(msl));
    assertNull(new LengthValidate("AB", 1L, null).getMessage(msl));
    assertNull(new LengthValidate("AB", 2L, null).getMessage(msl));

    // Hibás az adat
    assertNotNull(new LengthValidate(" ", 2L, 2L).getMessage(msl));
    assertNotNull(new LengthValidate("xyz", 7L, 2L).getMessage(msl));
    assertNotNull(new LengthValidate("AB", null, 1L).getMessage(msl));
    assertNotNull(new LengthValidate("AB", 3L, null).getMessage(msl));
    assertNotNull(new LengthValidate("AB", 3L, 10L).getMessage(msl));

  }
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new LengthValidate("abc", 5L, null).getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new LengthValidate("abc", null, 2L).getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
