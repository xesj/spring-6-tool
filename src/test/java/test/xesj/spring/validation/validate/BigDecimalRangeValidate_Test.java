package test.xesj.spring.validation.validate;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import static org.junit.jupiter.api.Assertions.*;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.validate.BigDecimalRangeValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * BigDecimalRangeValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class BigDecimalRangeValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    BigDecimal 
      middle = new BigDecimal("123.87"), 
      middle2 = new BigDecimal("123.87000"), 
      less = new BigDecimal("75.002"), 
      more = new BigDecimal("477.986");

    // Nincs adat    
    assertNull(new BigDecimalRangeValidate(null, less, less).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(null, null, less).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(null, less, null).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(null, null, null).getMessage(msl));
    assertTrue(new BigDecimalRangeValidate(null, null, null).isValid());

    // Helyes az adat
    assertNull(new BigDecimalRangeValidate(middle, middle, middle2).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(middle, less, more).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(middle, null, more).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(middle, null, middle).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(middle2, middle, null).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(middle, middle, more).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(more, more, more).getMessage(msl));
    assertNull(new BigDecimalRangeValidate(less, less, less).getMessage(msl));
    assertTrue(new BigDecimalRangeValidate(middle, middle, middle).isValid());

    // Hibás az adat
    assertNotNull(new BigDecimalRangeValidate(middle, more, less).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(middle, more, more).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(middle, less, less).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(middle, more, null).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(middle, null, less).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(less, middle, middle).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(less, middle, null).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(more, null, middle).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(more, less, middle).getMessage(msl));
    assertNotNull(new BigDecimalRangeValidate(more, middle, less).getMessage(msl));
    assertFalse(new BigDecimalRangeValidate(more, middle, less).isValid());

  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(
      new BigDecimalRangeValidate(
        new BigDecimal("5"), 
        new BigDecimal("7"), 
        null
      )
      .getMessage(msl)
      .startsWith(Message.NOT_EXIST)
    );
    assertFalse(
      new BigDecimalRangeValidate(
        new BigDecimal("5"), 
        null, 
        new BigDecimal("3")
      )
      .getMessage(msl)
      .startsWith(Message.NOT_EXIST)
    );

  }
  
  // ====
}
