package test.xesj.spring.validation.validate;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.validate.LongRangeValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * LongRangeValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class LongRangeValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    Long middle = 123L, less = 75L, more = 477L;

    // Nincs adat    
    assertNull(new LongRangeValidate(null, less, less).getMessage(msl));
    assertNull(new LongRangeValidate(null, null, less).getMessage(msl));
    assertNull(new LongRangeValidate(null, less, null).getMessage(msl));
    assertNull(new LongRangeValidate(null, null, null).getMessage(msl));
    assertTrue(new LongRangeValidate(null, null, null).isValid());

    // Helyes az adat
    assertNull(new LongRangeValidate(middle, middle, middle).getMessage(msl));
    assertNull(new LongRangeValidate(middle, less, more).getMessage(msl));
    assertNull(new LongRangeValidate(middle, null, more).getMessage(msl));
    assertNull(new LongRangeValidate(middle, null, middle).getMessage(msl));
    assertNull(new LongRangeValidate(middle, middle, null).getMessage(msl));
    assertNull(new LongRangeValidate(middle, middle, more).getMessage(msl));
    assertNull(new LongRangeValidate(more, more, more).getMessage(msl));
    assertNull(new LongRangeValidate(less, less, less).getMessage(msl));
    assertTrue(new LongRangeValidate(middle, middle, middle).isValid());

    // Hibás az adat
    assertNotNull(new LongRangeValidate(middle, more, less).getMessage(msl));
    assertNotNull(new LongRangeValidate(middle, more, more).getMessage(msl));
    assertNotNull(new LongRangeValidate(middle, less, less).getMessage(msl));
    assertNotNull(new LongRangeValidate(middle, more, null).getMessage(msl));
    assertNotNull(new LongRangeValidate(middle, null, less).getMessage(msl));
    assertNotNull(new LongRangeValidate(less, middle, middle).getMessage(msl));
    assertNotNull(new LongRangeValidate(less, middle, null).getMessage(msl));
    assertNotNull(new LongRangeValidate(more, null, middle).getMessage(msl));
    assertNotNull(new LongRangeValidate(more, less, middle).getMessage(msl));
    assertNotNull(new LongRangeValidate(more, middle, less).getMessage(msl));
    assertFalse(new LongRangeValidate(more, middle, less).isValid());

  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new LongRangeValidate(5L, 7L, null).getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new LongRangeValidate(5L, null, 3L).getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
