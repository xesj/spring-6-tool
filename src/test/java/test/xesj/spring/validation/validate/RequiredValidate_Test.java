package test.xesj.spring.validation.validate;
import test.xesj.spring.TestConfiguration;
import java.awt.Color;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.validate.RequiredValidate;
import xesj.tool.LocaleTool;

/**
 * RequiredValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class RequiredValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Helyes az adat
    assertNull(new RequiredValidate(" ", false).getMessage(msl));
    assertNull(new RequiredValidate("x").getMessage(msl));
    assertNull(new RequiredValidate("\uffff").getMessage(msl));
    assertNull(new RequiredValidate(0L).getMessage(msl));
    assertNull(new RequiredValidate(new Date()).getMessage(msl));
    assertNull(new RequiredValidate(new Object()).getMessage(msl));
    assertNull(new RequiredValidate(Color.RED).getMessage(msl));
    assertNull(new RequiredValidate("\r\n\t", false).getMessage(msl));

    // Hibás az adat
    assertNotNull(new RequiredValidate(null).getMessage(msl));
    assertNotNull(new RequiredValidate("").getMessage(msl));
    assertNotNull(new RequiredValidate(" ").getMessage(msl));
    assertNotNull(new RequiredValidate(" ", true).getMessage(msl));
    assertNotNull(new RequiredValidate(" \r \n \t ").getMessage(msl));

  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new RequiredValidate(null).getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new RequiredValidate(" ").getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
