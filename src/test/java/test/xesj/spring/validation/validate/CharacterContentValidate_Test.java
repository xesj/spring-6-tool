package test.xesj.spring.validation.validate;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.validate.CharacterContentValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * CharacterContentValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class CharacterContentValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
   
    // Nincs adat    
    assertNull(new CharacterContentValidate(null, "AB").getMessage(msl));
    assertNull(new CharacterContentValidate("", "AB").getMessage(msl));
    assertNull(new CharacterContentValidate("", "").getMessage(msl));
    
    // Helyes az adat
    assertNull(new CharacterContentValidate("100111010111", "01").getMessage(msl));
    assertNull(new CharacterContentValidate("100111010111", "00011").getMessage(msl));
    assertNull(new CharacterContentValidate("hj3jj3", "3jh").getMessage(msl));
    
    // Helyes az adat, nincsenek engedélyezett karakterek
    assertNull(new CharacterContentValidate(" ", "").getMessage(msl));
    assertNull(new CharacterContentValidate("xy", null).getMessage(msl));
    
    // Hibás az adat
    assertNotNull(new CharacterContentValidate("A", "B").getMessage(msl));
    assertNotNull(new CharacterContentValidate("hj3jj3", "jh").getMessage(msl));

  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new CharacterContentValidate("a", "b").getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
