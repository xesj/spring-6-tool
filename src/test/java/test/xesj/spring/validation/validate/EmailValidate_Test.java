package test.xesj.spring.validation.validate;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.validate.EmailValidate;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * EmailValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class EmailValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt. 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat    
    assertNull(new EmailValidate(null).getMessage(msl));
    assertNull(new EmailValidate("").getMessage(msl));
    assertTrue(new EmailValidate(null).isValid());
    assertTrue(new EmailValidate("").isValid());

    // Helyes az adat
    assertNull(new EmailValidate("valami@asdf.hu").getMessage(msl));
    assertNull(new EmailValidate("a@b.cd").getMessage(msl));
    assertTrue(new EmailValidate("valami@as.df.hu").isValid());

    // Hibás az adat
    assertNotNull(new EmailValidate(" ").getMessage(msl));
    assertNotNull(new EmailValidate("a@b.c").getMessage(msl));
    assertFalse(new EmailValidate("?").isValid());
    assertFalse(new EmailValidate("val@ami@asdf.hu").isValid());

  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new EmailValidate("rossz").getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new EmailValidate("@").getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
