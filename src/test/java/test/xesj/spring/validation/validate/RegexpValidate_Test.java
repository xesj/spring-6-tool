package test.xesj.spring.validation.validate;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.validate.RegexpValidate;
import xesj.spring.validation.validate.Validate;
import xesj.tool.LocaleTool;

/**
 * RegexpValidate Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class RegexpValidate_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat
    assertNull(new RegexpValidate(null, null).getMessage(msl));
    assertNull(new RegexpValidate(null, "abc").getMessage(msl));
    assertNull(new RegexpValidate("abc", null).getMessage(msl));

    assertNull(new RegexpValidate("", "").getMessage(msl));
    assertNull(new RegexpValidate("", "abc").getMessage(msl));
    assertNull(new RegexpValidate("abc", "").getMessage(msl));
    
    // Helyes az adat
    assertNull(new RegexpValidate("1234", "[0-4]{4}").getMessage(msl));
    assertNull(new RegexpValidate("AB876", "[BA]{2}876|xc").getMessage(msl));
    assertNull(new RegexpValidate("xc", "[BA]{2}876|xc").getMessage(msl));

    // Hibás az adat (hibaüzenet kiírással)
    Validate validate = new RegexpValidate("69?", "[0-9]{3}");
    assertNotNull(validate.getMessage(msl));

  }
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new RegexpValidate("a", "b").getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
