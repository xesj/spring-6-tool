package test.xesj.spring.validation;
import test.xesj.spring.validation.validate.TestValidateConcrete;
import test.xesj.spring.validation.validate.TestValidate;
import test.xesj.spring.validation.convert.TestConvert;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.ValidationUsageException;
import xesj.tool.LocaleTool;

/**
 * Az előállított hibaüzenet tesztelése attól függően hogy módosítjuk a hibaüzenetet prefix-szel, 
 * postfix-szel, és code plus-szal.
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class MessageCreate_Test {
  
  @Autowired MessageSource messageSource;
  
  private static final String 
    MESSAGE_CODE = "test.error",
    DATA = "ÁRVÍZTŰRŐ";

  /**
   * Alap hibaüzenet teszt 
   */
  @Test
  public void baseTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    String message1, message2, message3, message4;

    // Alap hibaüzenet 
    message1 = messageSource.getMessage(MESSAGE_CODE, new String[]{DATA}, LocaleTool.LOCALE_HU);
    message2 = new TestConvert(DATA).getMessage(msl);
    message3 = new TestValidate(DATA).getMessage(msl);
    message4 = new TestValidateConcrete(DATA).getMessage(msl);
    assertEquals(message1, message2);
    assertEquals(message1, message3);
    assertEquals(DATA, message4);

  }  
  
  /**
   * Exception teszt
   */
  @Test
  public void exceptionTest() {

    // A messageCode, messageText közül egyik megadása kötelező 
    assertThrows(
      ValidationUsageException.class,
      () -> { new Message(null, null, null); }
    );

    // Nem lehet egyszerre megadni a messageCode, messageText-et 
    assertThrows(
      ValidationUsageException.class,
      () -> { new Message("kód", null, "szöveg"); }     
    );

  }
  
  // ====
}
