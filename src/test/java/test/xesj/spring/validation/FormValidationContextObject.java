package test.xesj.spring.validation;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormValidationContextObject {
  
  private Integer r1; // Ebbe nem tud konvertálni a spring
  private Integer r2; // Ebbe nem tud konvertálni a spring

  private Integer szam;
  private Date datum;

}
