package test.xesj.spring.validation.convert;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.convert.Convert;
import xesj.spring.validation.convert.IntegerConvert;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.ValidationUsageException;
import xesj.tool.LocaleTool;
import xesj.tool.StringTool;

/**
 * IntegerConvert Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class IntegerConvert_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat
    assertNull(new IntegerConvert(null).getValue());
    assertNull(new IntegerConvert(null).getMessage(msl));
    assertTrue(new IntegerConvert(null).isValid());
    
    assertNull(new IntegerConvert("").getValue());
    assertNull(new IntegerConvert("").getMessage(msl));
    assertTrue(new IntegerConvert("").isValid());
    
    // Helyes az adat
    assertEquals(0, new IntegerConvert("0").getValue());
    assertNull(new IntegerConvert("0").getMessage(msl));
    assertTrue(new IntegerConvert("0").isValid());

    assertEquals(-123, new IntegerConvert("-123").getValue());
    assertNull(new IntegerConvert("-123").getMessage(msl));
    assertTrue(new IntegerConvert("-123").isValid());

    assertEquals(Integer.MIN_VALUE, new IntegerConvert(StringTool.str(Integer.MIN_VALUE)).getValue());
    assertNull(new IntegerConvert(StringTool.str(Integer.MIN_VALUE)).getMessage(msl));
    assertTrue(new IntegerConvert(StringTool.str(Integer.MIN_VALUE)).isValid());

    assertEquals(Integer.MAX_VALUE, new IntegerConvert(StringTool.str(Integer.MAX_VALUE)).getValue());
    assertNull(new IntegerConvert(StringTool.str(Integer.MAX_VALUE)).getMessage(msl));
    assertTrue(new IntegerConvert(StringTool.str(Integer.MAX_VALUE)).isValid());

    // Hibás az adat (hibaüzenet kiírással)
    String[] texts = {"xyz", "  7", "3 0", "3.1", "99999999999999999999", "X999999999Z99999999Y", "9999999999.999999999"}; 
    for (String text: texts) {
      Convert convert = new IntegerConvert(text);
      String message = convert.getMessage(msl); 
      assertNotNull(message);
      assertFalse(convert.isValid());
      assertThrows(
        ValidationUsageException.class,
        () -> { convert.getValue(); } 
      );
    }

  }

  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(new IntegerConvert("a").getMessage(msl).startsWith(Message.NOT_EXIST));
    assertFalse(new IntegerConvert("999999999999999999999999999999").getMessage(msl).startsWith(Message.NOT_EXIST));

  }
  
  // ====
}
