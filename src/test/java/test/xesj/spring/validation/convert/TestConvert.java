package test.xesj.spring.validation.convert;
import xesj.spring.validation.convert.Convert;
import xesj.spring.validation.Message;

/**
 * Teszt Convert
 */
public class TestConvert extends Convert {
  
  /**
   * Konstruktor
   */
  public TestConvert(String data) {

    message = new Message("test.error", new String[]{data}, null);

  }
  
  // ====
}
