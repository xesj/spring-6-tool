package test.xesj.spring.validation.convert;
import test.xesj.spring.TestConfiguration;
import java.text.ParseException;
import java.util.Date;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.convert.Convert;
import xesj.spring.validation.convert.DateConvert;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.DateTool;
import xesj.tool.LocaleTool;

/**
 * DateConvert Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class DateConvert_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() throws ParseException {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    String pattern1 = "yyyy.MM.dd";
    String evVegeStr = "2018.12.31";
    Date evVegeDate = DateTool.parse("2018.12.31", pattern1);

    // Nincs adat
    assertNull(new DateConvert(null, pattern1).getValue());
    assertNull(new DateConvert(null, pattern1).getMessage(msl));
    
    assertNull(new DateConvert("", pattern1).getValue());
    assertNull(new DateConvert("", pattern1).getMessage(msl));
    
    // Helyes az adat
    assertEquals(evVegeDate, new DateConvert(evVegeStr, pattern1).getValue());
    assertNull(new DateConvert(evVegeStr, pattern1).getMessage(msl));

    Convert dateConvert = new DateConvert("1968", "yyyy");
    assertEquals(DateTool.parse("1968", "yyyy"), (Date)dateConvert.getValue());
    assertNull(dateConvert.getMessage(msl));

    assertNull(new DateConvert("2016.02.29", pattern1).getMessage(msl));
    
    // Hibás az adat (hibaüzenet kiírással)
    assertNotNull(new DateConvert("2017.02.29", pattern1).getMessage(msl));

    dateConvert = new DateConvert("2018.01.01", "yyyy");
    assertNotNull(dateConvert.getMessage(msl));

  }  
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(
      new DateConvert("xxxx", "yyyy").getMessage(msl).startsWith(Message.NOT_EXIST)
    );

  }
  
  // ====
}
