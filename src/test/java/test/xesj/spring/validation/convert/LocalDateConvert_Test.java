package test.xesj.spring.validation.convert;
import test.xesj.spring.TestConfiguration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.convert.Convert;
import xesj.spring.validation.convert.LocalDateConvert;
import xesj.spring.validation.Message;
import xesj.spring.validation.MessageSourceLocale;
import xesj.tool.LocaleTool;

/**
 * LocalDateConvert Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class LocalDateConvert_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    String pattern1 = "uuuu.MM.dd";
    String evVegeStr = "2018.12.31";
    LocalDate evVegeDate = LocalDate.parse(
      "2018.12.31", 
      DateTimeFormatter.ofPattern(pattern1).withResolverStyle(ResolverStyle.STRICT)
    );

    // Nincs adat
    assertNull(new LocalDateConvert(null, pattern1).getValue());
    assertNull(new LocalDateConvert(null, pattern1).getMessage(msl));
    
    assertNull(new LocalDateConvert("", pattern1).getValue());
    assertNull(new LocalDateConvert("", pattern1).getMessage(msl));
    
    // Helyes az adat
    assertEquals(evVegeDate, new LocalDateConvert(evVegeStr, pattern1).getValue());
    assertNull(new LocalDateConvert(evVegeStr, pattern1).getMessage(msl));
    
    // Hibás az adat (hibaüzenet kiírással)
    assertNotNull(new LocalDateConvert("2017.02.29", pattern1).getMessage(msl));

    Convert convert = new LocalDateConvert("2018.01.01", "yyyy");
    assertNotNull(convert.getMessage(msl));

  }  
  
  /**
   * messages.properties teszt
   */
  @Test
  public void messagesPropertiesTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);
    
    // messages.properties bejegyzés meglétének ellenőrzése
    assertFalse(
      new LocalDateConvert("xxxx", "yyyy").getMessage(msl).startsWith(Message.NOT_EXIST)
    );

  }
  
  // ====
}
