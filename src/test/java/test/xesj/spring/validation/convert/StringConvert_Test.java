package test.xesj.spring.validation.convert;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import test.xesj.spring.TestConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import xesj.spring.validation.MessageSourceLocale;
import xesj.spring.validation.convert.StringConvert;
import xesj.tool.LocaleTool;

/**
 * StringConvert Teszt
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class StringConvert_Test {
  
  @Autowired MessageSource messageSource;

  /**
   * Hibaüzenet teszt 
   */
  @Test
  public void messageTest() {

    MessageSourceLocale msl = new MessageSourceLocale(messageSource, LocaleTool.LOCALE_HU);

    // Nincs adat
    assertNull(new StringConvert(null).getValue());
    assertNull(new StringConvert(null).getMessage(msl));
    
    assertNull(new StringConvert("").getValue());
    assertNull(new StringConvert("").getMessage(msl));
    
    // Van adat
    String[] texts = {" ", "éáű", "  \n \r \t ÉŐÚ   "};
    for (String text: texts) {
      assertEquals(text, new StringConvert(text).getValue());
      assertNull(new StringConvert(text).getMessage(msl));
    }

  }  
  
  // ====
}
